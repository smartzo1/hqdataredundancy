﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace TestingHQDataRedundancy
{
    static class Constants
    {
        public static string directoryCSV = "C:\\Users\\spyros.martzoukos\\Desktop\\HQdataRedundancy\\TestingHQDR\\TestingHQDR\\TestingHQDR\\bin\\Debug\\serviceInputFiles\\";
        public static string path2files = "C:\\Users\\spyros.martzoukos\\Desktop\\HQdataRedundancy\\TestingHQDR\\TestingHQDR\\TestingHQDR\\bin\\Debug";
        public const string connetionString = "User Id=userfoo;Password=1234;Data Source=//localhost:1521/xe";


        public const int sleepTime = 1000; // 1000 msec = 1 sec waiting time after deleting a csv.
        public const string dateFormat = "dd/MM/yyyy HH:mm:ss.fff";
        public const double rogueDefunct = -999; // Rogue data value when measurement device sends no signal.  
        public const double rogueReset = 0;  // Rogue data value when measurement device resets its operation.
        public const double loose = 0.9; // Relaxing the search space when investigating missing values. 0 < loose <= 1, only. Appropriate choice is 0.9
        public const int confidence = 3; // Extending the search space when investigating rogue values. Positive integer (could also be double). Appropriate choice is 2 or 3.
    }
    class ProgramExecute
    {
        //
        // This testing mechanism only checks for correct timestamp insertions into MAIN,
        // it does not test whether the corresponding values are what theys should be.
        // For non-counter-type (MAIN, RED) pairs, this is not an issue as it is highly unlikely
        // that incorrect measurement insertions will occur, while their corresponding timestamps
        // are the correct ones. For counter-type pairs, however, this IS important to check, but
        // the test is not currently supported. If you want to add such tests go to region
        // "compare" in compareServiceWithExpectedOutput.cs and add stuff.
        // Some related data structures already exist (Output_Value2Order, InBetweenValues...)
        // but are currently unused.
        //
        // You should create table TBL_DATA_RED in the database with 8 columns as explained in 
        // the beginning of generateDataBaseConjugatePairs.cs
        //
        // Under the "path2files" directory create the following folders
        //  "originalFiles"
        //  "serviceInputFiles"
        //  "serviceInputFiles_"
        //  "missingValues"
        //  "nonMissingValues"
        // (see Bar() for their explanation)
        //
        // ServiceDB.cs and ServiceCore.cs are the service's scripts as they should appear in HQ (with only slight modifications),
        // the remaining scripts of this solution are for testing purposes only.
        //

        static void Main()
        {
            Console.WriteLine("1) Generating conjugate pairs...");
            GenerateDataBaseConjugatePairs.Foo();

            Console.WriteLine("\n2) Generating CSVs for conjugate pairs...");
            GenerateCSVs.Bar();

            Console.WriteLine("\n3) Executing service for the following pairs:");
            Service.Moo();

            Console.WriteLine("\n4) Comparing service output with expected output....");
            int NoE = CompareServiceWithExpectedOutput.Zoo();
            Console.WriteLine("\n****** Number of errors = {0} *******",NoE);

            Console.WriteLine("\n\n\tTESTING ENDED. Hit Return to exit.");
            Console.ReadLine();
        }
    }
}
