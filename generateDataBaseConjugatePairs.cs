﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using System.Data.SqlTypes;
using Oracle.ManagedDataAccess.Client;
using System.IO;

namespace TestingHQDataRedundancy
{
    static public class GenerateDataBaseConjugatePairs
    {
        static public void Foo()
        {
            // SITE_ID    : vessel ID (int)
            // ID_MAIN    : MAIN Measurement Location ID (int)
            // ID_RED     : RED Measurement Location (int)
            // Q_MAIN     : "quax's 64bit address" + "_" + "quax's register ID" for MAIN (string)
            // Q_RED      : "quax's 64bit address" + "_" + "quax's register ID" for RED (string)
            // SR_MAIN    : MAIN's sampling rate in msec (int)
            // SR_RED     : RED's sampling rate in msec (int)          
            // IS_COUNTER : whether (MAIN, RED) is of counter-type, true/false (int, 0/1)

            int NoCP = 10; // # conjugate pairs to generate

            Random r = new Random();

            int SITE_ID = r.Next(1, 10);

            // int[] items = new int[100];
            int[] ID_MAIN = new int[NoCP];
            int[] ID_RED = new int[NoCP];
            string[] Q_MAIN = new string[NoCP];
            string[] Q_RED = new string[NoCP];
            int[] SR_MAIN = new int[NoCP];
            int[] SR_RED = new int[NoCP];
            int[] IS_COUNTER = new int[NoCP];

            for (int i = 0; i <= NoCP - 1; i++)
            {

                ID_MAIN[i] = i + 1;
                ID_RED[i] = 101 + i;

                int main_seconds = r.Next(60, 70);
                int red_seconds = r.Next(60, 70);

                SR_MAIN[i] = main_seconds * 1000;
                SR_RED[i] = red_seconds * 1000;

                string address = r.Next((int)Math.Pow(10, 7), (int)Math.Pow(10, 8)).ToString();
                string mainaddress = address + "MAIN";

                Q_MAIN[i] = Convert.ToString(mainaddress) + "_" + Convert.ToString(main_seconds);

                string redaddress = address + "RED";

                Q_RED[i] = Convert.ToString(redaddress) + "_" + Convert.ToString(red_seconds);

                IS_COUNTER[i] = r.Next(0, 2);

                // string print = ID_MAIN[i].ToString() + " | " + ID_RED[i].ToString() + " | " + Q_MAIN[i] + " | " + Q_RED[i] + " | " + SR_MAIN[i].ToString() + " | " + SR_RED[i].ToString() + " | " + IS_COUNTER[i].ToString();

                //Console.WriteLine("{0}", print);
            }

            using (StreamWriter file = new StreamWriter("conjugacy.csv"))
            {
                file.WriteLine("Q_MAIN,Q_RED,SR_MAIN,SR_RED,IS_COUNTER");
                for (int i = 0; i < NoCP; i++)
                {
                    file.WriteLine("{0},{1},{2},{3},{4}", Q_MAIN[i], Q_RED[i], SR_MAIN[i].ToString(), SR_RED[i].ToString(), IS_COUNTER[i].ToString());
                }
            }

            using (OracleConnection conn = new OracleConnection(Constants.connetionString))
            {
                conn.Open();

                OracleTransaction txn = conn.BeginTransaction(IsolationLevel.ReadCommitted);
                string sqlstr = @"INSERT INTO TBL_DATA_RED (ID_MAIN,ID_RED,Q_MAIN,Q_RED,SR_MAIN,SR_RED,IS_COUNTER)
                                                        VALUES (:p1,:p2,:p3,:p4,:p5,:p6,:p7)";

                OracleCommand com = new OracleCommand(sqlstr, conn);
                com.ArrayBindCount = NoCP;
                com.CommandType = CommandType.Text;
                com.CommandTimeout = 360;

                OracleParameter par1 = new OracleParameter("p1", OracleDbType.Varchar2);
                par1.Direction = ParameterDirection.Input;
                par1.Value = ID_MAIN;

                OracleParameter par2 = new OracleParameter("p2", OracleDbType.Varchar2);
                par2.Direction = ParameterDirection.Input;
                par2.Value = ID_RED;

                OracleParameter par3 = new OracleParameter("p3", OracleDbType.Varchar2);
                par3.Direction = ParameterDirection.Input;
                par3.Value = Q_MAIN;

                OracleParameter par4 = new OracleParameter("p4", OracleDbType.Varchar2);
                par4.Direction = ParameterDirection.Input;
                par4.Value = Q_RED;

                OracleParameter par5 = new OracleParameter("p5", OracleDbType.Int16);
                par5.Direction = ParameterDirection.Input;
                par5.Value = SR_MAIN;

                OracleParameter par6 = new OracleParameter("p6", OracleDbType.Int16);
                par6.Direction = ParameterDirection.Input;
                par6.Value = SR_RED;

                OracleParameter par7 = new OracleParameter("p7", OracleDbType.Int16);
                par7.Direction = ParameterDirection.Input;
                par7.Value = IS_COUNTER;

                com.Parameters.Add(par1);
                com.Parameters.Add(par2);
                com.Parameters.Add(par3);
                com.Parameters.Add(par4);
                com.Parameters.Add(par5);
                com.Parameters.Add(par6);
                com.Parameters.Add(par7);

                int num_values = com.ExecuteNonQuery();

                if (txn != null) txn.Commit();

                conn.Close();
            }
        }
    }
}