﻿





using System.Data;

using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using MathNet.Numerics.Distributions;
using System.Globalization;

namespace TestingHQDataRedundancy
{
    static public class GenerateCSVs
    {
        static public string csvName2dataBaseName(string csvName)
        {
            csvName = csvName.Replace("Quax_", "");
            csvName = csvName.Replace(".csv", "");
            string[] quaxinfo = Regex.Split(csvName, "_");
            return quaxinfo[0] + "_" + quaxinfo[2];
        }

        static public void WriteHashes(string directoryPath, Dictionary<string, Dictionary<string, DataTable>> D)
        {
            foreach (KeyValuePair<string, Dictionary<string, DataTable>> entry in D)
            {
                string qName = entry.Key;
                using (StreamWriter writer = new StreamWriter(directoryPath + "\\" + qName))
                {
                    foreach (KeyValuePair<string, DataTable> entry2 in D[qName])
                    {
                        writer.WriteLine("{0}", entry2.Key);
                        DataTable Table = entry2.Value;
                        for (int j = 0; j <= Table.Rows.Count - 1; j++)
                        {
                            string dateString = (Convert.ToDateTime(Table.Rows[j]["Date"])).ToString(Constants.dateFormat, CultureInfo.InvariantCulture);
                            double measurement = Convert.ToDouble(Table.Rows[j]["Measurement"]);
                            writer.WriteLine("{0},{1}", dateString, measurement.ToString());
                        }
                    }
                }

            }
        }

        static public void Bar()
        {

            // Folder containing all MAIN CSVs as originally generated (without deleting lines).
            // [*** No actual impact for testing, can be ignored ***] 
            string originalFiles = "originalFiles";

            // Folder containg a)RED CSVs; b) MAIN CSVs as in "originalFiles", but with deleted lines.
            string serviceInputFiles = "serviceInputFiles";

            // Copy of serviceInputFiles:
            string serviceInputFilesCopy = "serviceInputFiles_";

            // For each MAIN.csv of serviceInputFiles, the corresponding expected pairs of (time, value) that should be filled by its RED.
            string missingValuesFiles = "missingValues";

            // Similar to the above, but for time intervals (X_k, X_{k+1}) in MAIN for which we do not expect any insertion from RED.
            // [*** No actual impact for testing, can be ignored ***] 
            string nonMissingValuesFiles = "nonMissingValues";

            string[] filePathsCleanup = Directory.GetFiles(Constants.path2files + "\\" + originalFiles);
            foreach (string filePathCU in filePathsCleanup) { File.Delete(filePathCU); }

            filePathsCleanup = Directory.GetFiles(Constants.path2files + "\\" + serviceInputFiles);
            foreach (string filePathCU in filePathsCleanup) { File.Delete(filePathCU); }

            filePathsCleanup = Directory.GetFiles(Constants.path2files + "\\" + serviceInputFilesCopy);
            foreach (string filePathCU in filePathsCleanup) { File.Delete(filePathCU); }

            filePathsCleanup = Directory.GetFiles(Constants.path2files + "\\" + missingValuesFiles);
            foreach (string filePathCU in filePathsCleanup) { File.Delete(filePathCU); }

            filePathsCleanup = Directory.GetFiles(Constants.path2files + "\\" + nonMissingValuesFiles );
            foreach (string filePathCU in filePathsCleanup) { File.Delete(filePathCU); }

            List<string> Q_MAIN = new List<string>();
            Dictionary<string, string> Q_RED = new Dictionary<string, string>();
            Dictionary<string, double> SR_MAIN = new Dictionary<string, double>();
            Dictionary<string, double> SR_RED = new Dictionary<string, double>();
            Dictionary<string, int> IS_COUNTER = new Dictionary<string, int>();

            Dictionary<string, double> OriginalNoL = new Dictionary<string, double>();

            bool firstLine = true;
            string line;
            int i = 0;

            using (StreamReader file = new StreamReader("conjugacy.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (firstLine)
                    {
                        firstLine = false;
                        continue;
                    }
                    string[] data = Regex.Split(line, ",");
                    //"Q_MAIN,Q_RED,SR_MAIN,SR_RED,IS_COUNTER"
                    Q_MAIN.Add(data[0]);
                    Q_RED.Add(data[0], data[1]);
                    SR_MAIN.Add(data[0], (Convert.ToDouble(data[2]) / 1000) / 60);
                    SR_RED.Add(data[1], (Convert.ToDouble(data[3]) / 1000) / 60);
                    IS_COUNTER.Add(data[0], Convert.ToInt32(data[4]));

                    OriginalNoL.Add(data[0], Poisson.Sample(60 / SR_MAIN[Q_MAIN[i]]));

                    i++;
                }
            }
            double NoCP = (double)i;
            
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // Create Q_MAIN CSVs//
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////

            Random r = new Random();

            DateTime date = new DateTime(1950, 1, 1, 0, 0, 0, 0);
            //Console.WriteLine(baseDatetime.ToString(dateFormat, CultureInfo.InvariantCulture));

            Dictionary<string, List<DateTime>> Boundary = new Dictionary<string, List<DateTime>>();


            for (i = 0; i < NoCP; i++)
            {
                string[] quaxinfo = Regex.Split(Q_MAIN[i], "_");
                string csvFile = "Quax" + "_" + quaxinfo[0] + "_ID_" + quaxinfo[1] + ".csv";
                string qName = csvName2dataBaseName(csvFile);

                DateTime X_ini = DateTime.Now;
                DateTime X_fin = DateTime.Now;

                double measurement = 0;
                double prev_measurement = 0;

                using (StreamWriter writer = new StreamWriter(Constants.path2files + "\\" + originalFiles + "\\" + csvFile))
                {
                    for (int j = 0; j < OriginalNoL[Q_MAIN[i]]; j++)
                    {
                        double epsilon = Normal.Sample(0, Math.Pow(SR_MAIN[qName], 2));

                        double DT = Math.Abs(SR_MAIN[qName] + epsilon);
                        date = date.AddMinutes(DT);


                        if (IS_COUNTER[qName] == 0)
                        {
                            measurement = Math.Round(r.NextDouble(), 4);
                        }
                        else
                        {
                            measurement = Math.Round(prev_measurement + r.NextDouble(), 4);
                        }
                        //Console.WriteLine("{0} ---- {1} ---- {2}", date.ToString(dateFormat, CultureInfo.InvariantCulture), epsilon.ToString(), DT.ToString());
                        //Console.ReadLine();
                        writer.WriteLine("{0},{1}", date.ToString(Constants.dateFormat, CultureInfo.InvariantCulture), measurement.ToString());

                        if (j == 0) { X_ini = date; }
                        if (j == OriginalNoL[Q_MAIN[i]] - 1) { X_fin = date; }

                        prev_measurement = measurement;
                    }
                }

                Boundary.Add(Q_MAIN[i], new List<DateTime> { X_ini, X_fin });
            }
            //Console.WriteLine(value.ToString());
            //Console.ReadLine();

            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // Delete lines form Q_MAIN CSVs//
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////

            Dictionary<string, double> AfterDeletionNoL = new Dictionary<string, double>();

            string targetDirectory = Constants.path2files + "\\" + originalFiles + "\\";
            string[] originalFilesPaths = Directory.GetFiles(targetDirectory, "*.csv", SearchOption.AllDirectories);

            foreach (string path in originalFilesPaths)
            {
                string csvName = path.Replace(targetDirectory, "");

                i = 0;

                using (StreamWriter writer = new StreamWriter(Constants.path2files + "\\" + serviceInputFiles + "\\" + csvName))
                {
                    using (StreamReader reader = new StreamReader(path))
                    {
                        while ((line = reader.ReadLine()) != null)
                        {
                            double p = r.NextDouble();
                            if ((0 <= p) && (p < 0.3))
                            {
                                writer.WriteLine("{0}", line);
                                i++;
                            }
                        }
                    }
                }

                AfterDeletionNoL.Add(csvName2dataBaseName(csvName), i);
            }

            foreach (var file in Directory.GetFiles(serviceInputFiles))
            {
                File.Copy(file, Path.Combine(serviceInputFilesCopy, Path.GetFileName(file)));
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // Check for missing values //
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////


            Dictionary<string, List<string>> CheckTheseIntervalsForMissingValues = new Dictionary<string, List<string>>();

            targetDirectory = Constants.path2files + "\\" + serviceInputFiles + "\\";
            string[] serviceInputFilesPaths = Directory.GetFiles(targetDirectory, "*.csv", SearchOption.AllDirectories);

            foreach (string path in serviceInputFilesPaths)
            {
                string csvName = path.Replace(targetDirectory, "");
                string qName = csvName2dataBaseName(csvName);
                i = 0;

                DateTime dateTime = DateTime.Now;
                double measurement = 0;
                DateTime prev_dateTime = DateTime.Now;
                double prev_measurement = 0;

                using (StreamReader reader = new StreamReader(path))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] data = Regex.Split(line, ",");

                        dateTime = Convert.ToDateTime(data[0]);
                        measurement = Convert.ToDouble(data[1]);

                        if (i == 0) {
                            if (dateTime != Boundary[qName][0])
                            {
                                string interval = DateTime.MinValue.ToString(Constants.dateFormat, CultureInfo.InvariantCulture) + "_" + dateTime.ToString(Constants.dateFormat, CultureInfo.InvariantCulture);
                                //string interval = "MININF" + "_" + dateTime.ToString(dateFormat, CultureInfo.InvariantCulture);
                                CheckTheseIntervalsForMissingValues.Add(qName, new List<string>());
                                CheckTheseIntervalsForMissingValues[qName].Add(interval);
                            }
                        }
                        else if(i == AfterDeletionNoL[qName] - 1)
                        {
                            if (dateTime != Boundary[qName][1])
                            {
                                string interval =  dateTime.ToString(Constants.dateFormat, CultureInfo.InvariantCulture) + "_" + DateTime.MaxValue.ToString(Constants.dateFormat, CultureInfo.InvariantCulture);
                                //string interval = dateTime.ToString(dateFormat, CultureInfo.InvariantCulture) + "_" + "PLUSINF";
                                if (!CheckTheseIntervalsForMissingValues.ContainsKey(qName))
                                {
                                    CheckTheseIntervalsForMissingValues.Add(qName, new List<string>());
                                }
                                CheckTheseIntervalsForMissingValues[qName].Add(interval);
                            }
                        }
                        else
                        {
                            if ( (dateTime - prev_dateTime).TotalMinutes >= 2 * SR_MAIN[qName] )
                            {
                                string interval = prev_dateTime.ToString(Constants.dateFormat, CultureInfo.InvariantCulture) + "_" + dateTime.ToString(Constants.dateFormat, CultureInfo.InvariantCulture);
                                if (!CheckTheseIntervalsForMissingValues.ContainsKey(qName))
                                {
                                    CheckTheseIntervalsForMissingValues.Add(qName, new List<string>());
                                }
                                CheckTheseIntervalsForMissingValues[qName].Add(interval);
                            }
                        }

                        prev_dateTime = dateTime;
                        prev_measurement = measurement;
                        i++;
                     }
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // Create Q_RED //
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////

            Dictionary<string, Dictionary<string, DataTable>> MissingValues = new Dictionary<string, Dictionary<string, DataTable>>();

            Dictionary<string, Dictionary<string, DataTable>> NonMissingValues = new Dictionary<string, Dictionary<string, DataTable>>();

            foreach (KeyValuePair<string, List<string>> entry in CheckTheseIntervalsForMissingValues)
            {

                string qName = entry.Key;
                List<string> list_intervals = entry.Value;

                string red_qName = Q_RED[qName];
                string[] quaxinfo = Regex.Split(red_qName, "_");
                string red_csvName = "Quax_" + quaxinfo[0] + "_ID_" + quaxinfo[1] + ".csv";

                targetDirectory = Constants.path2files + "\\" + serviceInputFiles + "\\";

                double red_prevMeasurement = 0;
                double red_currMeasurement = 0;

                DateTime red_prevTESTDate = DateTime.MinValue;
                DateTime red_currTESTDate = DateTime.MinValue;

                DateTime red_prevDate = DateTime.MinValue;
                DateTime red_currDate = DateTime.MinValue;

                MissingValues.Add(qName, new Dictionary<string, DataTable>());
                NonMissingValues.Add(qName, new Dictionary<string, DataTable>());

                using (StreamWriter writer = new StreamWriter(targetDirectory + red_csvName))
                {

                    for (int j = 0; j <= list_intervals.Count - 1; j++)
                    {
                        string[] stringdates = Regex.Split(list_intervals[j], "_");
                        DateTime prevDate = Convert.ToDateTime(stringdates[0]);
                        DateTime currDate = Convert.ToDateTime(stringdates[1]);

                        if ( ((prevDate - DateTime.MinValue).Duration()).TotalMinutes < 1)
                        {
                            prevDate = currDate.AddMinutes(-5); 
                        }
                        else if (((currDate - DateTime.MaxValue).Duration()).TotalMinutes < 1)
                        {
                            currDate = prevDate.AddMinutes(+5);
                        }

                        bool start = true;

                        MissingValues[qName].Add(list_intervals[j], new DataTable());
                        MissingValues[qName][list_intervals[j]].Columns.Add("Date", typeof(DateTime));
                        MissingValues[qName][list_intervals[j]].Columns.Add("Measurement", typeof(double));

                        NonMissingValues[qName].Add(list_intervals[j], new DataTable());
                        NonMissingValues[qName][list_intervals[j]].Columns.Add("Date", typeof(DateTime));
                        NonMissingValues[qName][list_intervals[j]].Columns.Add("Measurement", typeof(double));

                        while ((red_prevTESTDate < currDate) && (red_currTESTDate < currDate))
                        {
                            double epsilon = Normal.Sample(0, Math.Pow(Constants.loose, 1));
                            double DT = Math.Abs(Constants.loose * SR_MAIN[qName] + epsilon);

                            if (start)
                            {
                                red_prevTESTDate = prevDate.AddMinutes(DT);
                                start = false;
                            }
                            else
                            {
                                red_prevTESTDate = red_currTESTDate.AddMinutes(DT);
                            }

                            if (IS_COUNTER[qName] == 0)
                            {
                                red_prevMeasurement = Math.Round(10 * r.NextDouble(), 4);
                                red_currMeasurement = Math.Round(10 * r.NextDouble(), 4);
                            }
                            else
                            {
                                red_prevMeasurement = Math.Round(red_prevMeasurement + 1000 * r.NextDouble(), 4);
                                red_currMeasurement = Math.Round(red_prevMeasurement + 1000 * r.NextDouble(), 4);
                            }

                            if (red_prevTESTDate < currDate)
                            {
                                red_prevDate = red_prevTESTDate;
                                writer.WriteLine("{0},{1}", red_prevDate.ToString(Constants.dateFormat, CultureInfo.InvariantCulture), red_prevMeasurement.ToString());

                                bool lower_bound_satisfied = ((prevDate - red_prevDate).Duration()).TotalMinutes > Constants.loose * SR_MAIN[qName];
                                bool upper_bound_satisfied = ((currDate - red_prevDate).Duration()).TotalMinutes > Constants.loose * SR_MAIN[qName];

                                if (lower_bound_satisfied && upper_bound_satisfied)
                                {
                                    MissingValues[qName][list_intervals[j]].Rows.Add(red_prevDate, red_prevMeasurement);
                                }
                                else
                                {
                                    NonMissingValues[qName][list_intervals[j]].Rows.Add(red_prevDate, red_prevMeasurement);
                                }

                            }

                            epsilon = Normal.Sample(0, Math.Pow(SR_RED[red_qName], 1));
                            DT = Math.Abs(SR_RED[red_qName] + epsilon);
                            red_currTESTDate = red_prevTESTDate.AddMinutes(DT);

                            if (red_currTESTDate < currDate)
                            {
                                red_currDate = red_currTESTDate;
                                writer.WriteLine("{0},{1}", red_currDate.ToString(Constants.dateFormat, CultureInfo.InvariantCulture), red_currMeasurement.ToString());

                                bool lower_bound_satisfied = ((prevDate - red_currDate).Duration()).TotalMinutes > Constants.loose * SR_MAIN[qName];
                                bool upper_bound_satisfied = ((currDate - red_currDate).Duration()).TotalMinutes > Constants.loose * SR_MAIN[qName];

                                if (lower_bound_satisfied && upper_bound_satisfied)
                                {
                                    MissingValues[qName][list_intervals[j]].Rows.Add(red_currDate, red_currMeasurement);
                                }
                                else
                                {
                                    NonMissingValues[qName][list_intervals[j]].Rows.Add(red_currDate, red_currMeasurement);
                                }
                            }
                        }

                    }
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // Write  MissingValues and NonMissingValues to  files//
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////

            string directoryPath = Constants.path2files + "\\" + missingValuesFiles;
            WriteHashes(directoryPath, MissingValues);

            directoryPath = Constants.path2files + "\\" + nonMissingValuesFiles;
            WriteHashes(directoryPath, NonMissingValues);

            }
    }
}
