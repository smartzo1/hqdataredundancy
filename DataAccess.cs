﻿using System;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;



namespace HQDataRedundancy
{
    static public class DataAccess
    {
        static public void ReadLexiconsInMemory(string DbConnString, Dictionary<string, List<string>> LexiconMain, Dictionary<string, List<string>> LexiconRed)
        //*** static public void ReadLexiconsInMemory(Dictionary<string, List<string>> LexiconMain, Dictionary<string, List<string>> LexiconRed)
        {

            //*** switch (DbType)
            //*** {
            //*** case DbType.ORACLE:

            OracleDataReader reader = null;
            try
            {
                string Orasqlstr = @"SELECT Q_MAIN, Q_RED, SR_MAIN, SR_RED, IS_COUNTER FROM TBL_DATA_RED";
                using (OracleConnection conn = new OracleConnection(DbConnString))
                {
                    conn.Open();
                    OracleCommand cmd = new OracleCommand(Orasqlstr, conn);
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        string q_main = reader["Q_MAIN"].ToString();
                        string sr_main = reader["SR_MAIN"].ToString();
                        string q_red = reader["Q_RED"].ToString();
                        string sr_red = reader["SR_RED"].ToString();
                        string is_counter = reader["IS_COUNTER"].ToString();

                        LexiconMain.Add(q_main, new List<string> { q_red, sr_main, is_counter });
                        LexiconRed.Add(q_red, new List<string> { q_main, sr_red, is_counter });
                    }
                    conn.Close();
                }
            }
            catch (OracleException oex)
            {
                //*** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DH-RED0000: Error to get the Data redundancy from Oracle - " + oex.Message));
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
            //*** break;

            //case DbType.SQL:
            //    SqlDataReader SqlReader = null;
            //    try
            //    {
            //        string sqlstr = @"SELECT Q_MAIN, Q_RED, SR_MAIN, SR_RED, IS_COUNTER FROM TBL_DATA_RED";
            //        using (SqlConnection conn = new SqlConnection(DbConnString))
            //        {
            //            conn.Open();
            //            SqlCommand cmd = new SqlCommand(sqlstr, conn);
            //            SqlReader = cmd.ExecuteReader();

            //            while (SqlReader.Read())
            //            {
            //                string q_main = reader["Q_MAIN"].ToString();
            //                string sr_main = reader["SR_MAIN"].ToString();
            //                string q_red = reader["Q_RED"].ToString();
            //                string sr_red = reader["SR_RED"].ToString();
            //                string is_counter = reader["IS_COUNTER"].ToString();
            //
            //                LexiconMain.Add(q_main, new List<string> { q_red, sr_main, is_counter } );
            //                LexiconRed.Add(q_red, new List<string> { q_main, sr_red, is_counter } );
            //            }
            //            conn.Close();
            //        }
            //    }
            //    catch (SqlException sqlex)
            //    {
            //        if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DH-RED0001: Error to get the Data redundancy from SQL - " + sqlex.Message));
            //    }
            //    finally
            //    {
            //        if (SqlReader != null)
            //        {
            //            SqlReader.Close();
            //        }
            //    }
            //    break;
            // }
        }

    }
}
