﻿using System.Data;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using MathNet.Numerics.Distributions;
using System.Globalization;

namespace TestingHQDataRedundancy
{
    static public class CompareServiceWithExpectedOutput
    {

        static public void ReadFiles(string[] filesPaths, string targetDirectory, Dictionary<string, Dictionary<string, DataTable>> D)
        {
            string line;
            string interval = null;

            foreach (string pathToFile in filesPaths)
            {
                string qName = pathToFile.Replace(targetDirectory, "");
                D.Add(qName, new Dictionary<string, DataTable>());

                using (StreamReader reader = new StreamReader(pathToFile))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Contains("_"))
                        {
                            interval = line;
                            D[qName].Add(line, new DataTable());
                            D[qName][interval].Columns.Add("Date", typeof(DateTime));
                            D[qName][interval].Columns.Add("Measurement", typeof(double));
                            // 02/01/1950 05:37:30.643_02/01/1950 05:52:25.020
                            // 02 / 01 / 1950 05:38:59.299,6.6098
                            // 02 / 01 / 1950 05:41:18.218,6.9476
                            // 02 / 01 / 1950 05:41:23.617,3.7426
                        }
                        else
                        {
                            string[] data = Regex.Split(line, ",");
                            DateTime date = Convert.ToDateTime(data[0]);
                            double measurement = Convert.ToDouble(data[1]);
                            D[qName][interval].Rows.Add(date, measurement);
                        }
                    }
                }
            }
        }

        static public int Zoo()
        {
            string path2files = "C:\\Users\\spyros.martzoukos\\Desktop\\HQdataRedundancy\\TestingHQDR\\TestingHQDR\\TestingHQDR\\bin\\Debug";
            string serviceInputFiles = "serviceInputFiles";
            string missingValuesHashes = "missingValues";

            //
            // This script tests only for appropriate datetime insertions of missing values from RED into MAIN .
            // It does not test the validity of the corresponding values:
            // This can be trivially extended for the non-counter case, but requires more work for the
            // counter-type case.
            //


            //////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////

            Dictionary<string, Dictionary<string, DataTable>> MissingValues = new Dictionary<string, Dictionary<string, DataTable>>();

            string targetDirectory = path2files + "\\" + missingValuesHashes + "\\";
            string[] filesPaths = Directory.GetFiles(targetDirectory);

            ReadFiles(filesPaths, targetDirectory, MissingValues);

            //////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////

            targetDirectory = path2files + "\\" + serviceInputFiles + "\\";
            filesPaths = Directory.GetFiles(targetDirectory);
            string line;
            int NoE = 0;

            foreach (string pathToFile in filesPaths)
            {
                string qName = pathToFile.Replace(targetDirectory, "");
                qName = qName.Replace("Quax_", "");
                qName = qName.Replace("_ID_", "_");
                qName = qName.Replace(".csv", "");

                if (qName.Contains("RED")) { continue; }

                int i = 0;
                Dictionary<DateTime, int> Output_Date2Order = new Dictionary<DateTime, int>();
                Dictionary<int, DateTime> Output_Order2Date = new Dictionary<int, DateTime>();
                Dictionary<double, int> Output_Value2Order = new Dictionary<double, int>();
                Dictionary<int, double> Output_Order2Value = new Dictionary<int, double>();

                using (StreamReader reader = new StreamReader(pathToFile))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] data = Regex.Split(line, ",");
                        DateTime date = Convert.ToDateTime(data[0]);
                        double measurement = Convert.ToDouble(data[1]);

                        Output_Date2Order[date] = i;
                        Output_Order2Date[i] = date;
                        Output_Value2Order[measurement] = i;
                        Output_Order2Value[i] = measurement;

                        i++;
                    }
                }

                foreach (KeyValuePair<string, DataTable> entry in MissingValues[qName])
                {
                    string[] stringdates = Regex.Split(entry.Key, "_");
                    DateTime fromDate = Convert.ToDateTime(stringdates[0]);
                    DateTime toDate = Convert.ToDateTime(stringdates[1]);

                    DataTable D = entry.Value;

                    DateTime[] InBetweenDates = new DateTime[D.Rows.Count];
                    double[] InBetweenValues = new double[D.Rows.Count];

                    for (int k = 0; k <= D.Rows.Count - 1; k++)
                    {
                        InBetweenDates[k] = Convert.ToDateTime(D.Rows[k]["Date"]);
                        InBetweenValues[k] = Convert.ToDouble(D.Rows[k]["Measurement"]);
                    }

                    #region compare

                    if (!Output_Date2Order.ContainsKey(fromDate))
                    {
                        double timeDiff = ((fromDate - DateTime.MinValue).Duration()).TotalMinutes;
                        if (timeDiff > 1)
                        {
                            Console.WriteLine("ERROR: In MAIN, {0} has not been considered by the service as timestamp X_k for time interval (X_k, X_{k+1}) in which values should be inserted by RED.", fromDate.ToString(Constants.dateFormat, CultureInfo.InvariantCulture));
                            NoE++;
                        }
                        continue;
                    }
                    else if (!Output_Date2Order.ContainsKey(toDate))
                    {
                        double timeDiff = ((toDate - DateTime.MaxValue).Duration()).TotalMinutes;
                        if (timeDiff > 1)
                        {
                            Console.WriteLine("ERROR: In MAIN, {0} has not been considered by the service as timestamp X_{k+1} for time interval (X_k, X_{k+1}) in which values should be inserted by RED.", toDate.ToString(Constants.dateFormat, CultureInfo.InvariantCulture));
                            NoE++;
                        }
                        continue;
                    }

                    for (int j = Output_Date2Order[fromDate] + 1; j < Output_Date2Order[toDate]; j++)
                    {
                        int k = j - Output_Date2Order[fromDate] - 1;

                        try
                        {
                            double timeDiff = ((Output_Order2Date[j] - InBetweenDates[k]).Duration()).TotalMinutes;

                            if (timeDiff > 0.01)
                            {
                                Console.WriteLine("ERROR: inserted {0} instead of {1}", Output_Order2Date[j], InBetweenDates[k]);
                                NoE++;
                            }
                        }
                        catch
                        {
                            NoE++;
                        }
                    }

                    #endregion compare
                }

            }

            return NoE;







        }
    }
}








     
