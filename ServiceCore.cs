using System.Data;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;
using System.Threading;
using System.Globalization;

namespace TestingHQDataRedundancy

{

    static class Support
    {
        static public void WriteLine(StreamWriter file, DataTable dt, int n)
        {
            double measurement = Convert.ToDouble(dt.Rows[n]["Measurement"]);

            string date = (Convert.ToDateTime(dt.Rows[n]["Date"])).ToString(Constants.dateFormat, CultureInfo.InvariantCulture);

            file.WriteLine("{0},{1}", date, measurement.ToString());
        }

        static public void WriteCounterLine(StreamWriter file, MeasurementLocation Left, Dictionary<int,double> baseRightValues, int n)
        {
            double baseLeftMeasurement  = Convert.ToDouble(Left.Table.Rows[Left.Index - 1]["Measurement"]);
            double baseRightMeasurement = baseRightValues[Left.Index - 1];
            double rightMeasurement = Convert.ToDouble(Left.Candidates[Left.Index].Rows[n]["Measurement"]);

            // Ensure that none of the right measurements are rogue values.
            if ( (baseLeftMeasurement != Constants.rogueReset) && ( (baseRightMeasurement == Constants.rogueReset) || (rightMeasurement == Constants.rogueReset) ) )
            {
                return;
            }

            string date = (Convert.ToDateTime(Left.Candidates[Left.Index].Rows[n]["Date"])).ToString(Constants.dateFormat, CultureInfo.InvariantCulture);
            double measurement = baseLeftMeasurement + rightMeasurement - baseRightMeasurement;

            file.WriteLine("{0},{1}", date, measurement.ToString());
        }
        static public void SetTableSchema(DataTable dt)
        {
            dt.Columns.Add("Date", typeof(DateTime));
            dt.Columns.Add("Measurement", typeof(double));
        }

        static public string Name2Path(string name)
        {
            string[] quaxinfo = Regex.Split(name, "_");
            return Constants.directoryCSV + "Quax_" + quaxinfo[0] + "_ID_" + quaxinfo[1] + ".csv";
        }

        static public string Path2Name(string path)
        {
            string dummy = path.Replace(".csv", "");
            //dummy = dummy.Replace(Parameters.directoryCSV + "\\Quax_", "");
            dummy = dummy.Replace(Constants.directoryCSV + "Quax_", "");
            string[] quaxinfo = Regex.Split(dummy, "_ID_");
            return quaxinfo[0] + "_" + quaxinfo[1];
        }

        static public int Modulo(int x, int m)
        {
            return (x % m + m) % m;
        }

    }

    public class Conjugacy
    {

        #region Events
        // *** public static event EventHandler<LogEventArgs> LogMessage;
        #endregion

        /// <summary>
        /// This class is responsible for handling MAIN and RED CSVs, but NOT their content.
        /// </summary>

        //  Lexicon[LEFT][0] = RIGHT.
        //  Lexicon[LEFT][1] = samplingRate_LEFT
        //  Lexicon[LEFT][2] = is_counter
        public Dictionary<string, List<string>> Lexicon = new Dictionary<string, List<string>>();
        public string CSVname { get; set; }

        // Holds the names of all CSVs in directoryCSV. ActiveCSV[name] = "1" (value is always "1").
        public static Dictionary<string, string> ActiveCSV = new Dictionary<string, string>();

        static public void IdentifyConjugate(string name, Dictionary<string, string> ActiveCSV, Dictionary<string, string> AvoidDuplicates, Conjugacy left, Conjugacy right)
        {
            left.CSVname = name;
            right.CSVname = left.Lexicon[name][0];

            if (ActiveCSV.ContainsKey(right.CSVname))
            {
                // If conjugate csv right.CSVname exists in ActiveCSV then simply update AvoidDuplicates
                // (so that when right.name appears later when reading the directoryCSV, it won't be processed again).
                AvoidDuplicates.Add(right.CSVname, "1");
            }
            else
            {
                try
                {
                    // Otherwise, right.CSVname does not exist at all in the directoryCSV: 
                    // Create an empty csv file with the name right.CSVname anyway.
                    // This step only eases up the execution of subsequent steps. 
                    File.Create(Support.Name2Path(right.CSVname)).Dispose();
                    // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.MESSAGE, "CONJ-00000: The .csv file" + "  " + right.CSVname.ToString() + "  " + "was created, because it did not exist"));

                }
                catch (Exception ex)
                {
                    // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "CONJ-00000: Cannot create CSV file - " + ex.Message));
                }
            }
        }
    }

    public class MeasurementLocation
    {
        /// <summary>
        /// This class is responsible for handling the content of MAIN and RED CSVs.
        /// </summary>
        /// 

        public DataTable Table = new DataTable();
        public int Index { get; set; }
        public DateTime CurrDate { get; set; }
        public DateTime PrevDate { get; set; }
        public double CurrValue { get; set; }
        public double PrevValue { get; set; }
        public double SamplingRate { get; set; } // Sampling rate of Measurement Location in minutes.

        public static bool IsCounter = false; // Static because if MAIN is of counter-type, then so is RED and vice versa.

        // An Index (key) points to a DataTable of candidate entries from its conjugate. 
        public Dictionary<int, DataTable> Candidates = new Dictionary<int, DataTable>();

        static public void ReadAndStoreCSV(string name, DataTable mlTable)
        {
            string pathToFile = Support.Name2Path(name);

            using (StreamReader sreader = File.OpenText(pathToFile))
            {
                string line = String.Empty;
                while ((line = sreader.ReadLine()) != null)
                {
                    string[] data = Regex.Split(line, ",");
                    DateTime date = DateTime.ParseExact(data[0], Constants.dateFormat, CultureInfo.InvariantCulture);
                    double measurement = Convert.ToDouble(data[1]);
                    if (measurement == Constants.rogueDefunct) { continue; }
                    mlTable.Rows.Add(date, measurement);
                }
            }
        }
    }

    static class MainAlgorithm
    {
        static public void SetLeftDate(MeasurementLocation Left)
        {
            // Set the time interval for Left:
            if (Left.Index == 0)
            {
                // Start: (-infinity, time_initial) 
                Left.PrevDate = DateTime.MinValue;
                Left.CurrDate = Convert.ToDateTime(Left.Table.Rows[Left.Index]["Date"]);
            }
            else if (Left.Index == Left.Table.Rows.Count)
            {
                // End: (time_final, +infinity)
                Left.PrevDate = Convert.ToDateTime(Left.Table.Rows[Left.Index - 1]["Date"]);
                Left.CurrDate = DateTime.MaxValue;
            }
            else
            {
                // (time_{k-1}, time_k)
                Left.PrevDate = Convert.ToDateTime(Left.Table.Rows[Left.Index - 1]["Date"]);
                Left.CurrDate = Convert.ToDateTime(Left.Table.Rows[Left.Index]["Date"]);
            }
        }

        static public bool MissingValuesExist(MeasurementLocation Left)
        {
            if ((Left.CurrDate - Left.PrevDate).TotalMinutes < 2 * Left.SamplingRate)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        static public void SetRightDate(MeasurementLocation Left, MeasurementLocation Right)
        {
            // Set Right.CurrDate by finding appropriate index Right.Index such that 
            // Left.CurrDate > Right.CurrDate (at that Right.Index) 
            // Left.CurrDate < Right.CurrDate (at Right.Index + 1).
            // In other words, find index Right.Index such that, in the Right timespan, 
            // Right.CurrDate is a date immediately before date Left.CurrDate. 

            Right.CurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
            while (Right.CurrDate < Left.CurrDate)
            {
                if (Right.Index == Right.Table.Rows.Count - 1) { break; }
                Right.Index++;
                Right.CurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
                if (Right.CurrDate > Left.CurrDate)
                {
                    Right.Index--;
                    Right.CurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
                    break;
                }
            }
        }

        static public bool ProceedToSearch(MeasurementLocation Left, MeasurementLocation Right)
        {
            if ((Left.PrevDate >= Right.CurrDate) || (Right.CurrDate >= Left.CurrDate))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        static public void ScanRightForMissingValues(MeasurementLocation Left, MeasurementLocation Right)
        {
            DateTime ScanDate = Right.CurrDate;
            int ScanIndex = Right.Index;
            bool isInitialized = false;

            // Starting from Right.CurrDate, scan Right backwards until you exceed Left.PrevDate.
            // In other words, stay within the time interval as in ProceedToSearch() and compare
            // Left and Right dates. 
            // 
            // If a ScanDate is distant enough from Left.CurrDate and Left.PrevDate, then the line
            // in Right that corresponds to ScanDate is added to Left's candidate list.
            //
            // The loose factor (0 < loose <= 1) relaxes the search space for an appropriate (missing) Right date. 

            while (ScanDate > Left.PrevDate)
            {
                bool satisfy_lower_bound = (ScanDate - Left.PrevDate).TotalMinutes > Constants.loose * Left.SamplingRate;
                bool satisfy_upper_bound = (Left.CurrDate - ScanDate).TotalMinutes > Constants.loose * Left.SamplingRate;

                if (satisfy_lower_bound && satisfy_upper_bound)
                {
                    if (!isInitialized)
                    {
                        Left.Candidates.Add(Left.Index, new DataTable());
                        Support.SetTableSchema(Left.Candidates[Left.Index]);
                        Left.Candidates[Left.Index].Columns.Add("ConjugateIndex", typeof(int));
                        isInitialized = true;
                    }

                    double measurement = Convert.ToDouble(Right.Table.Rows[ScanIndex]["Measurement"]);

                    Left.Candidates[Left.Index].Rows.Add(ScanDate, measurement, ScanIndex);
                }

                ScanIndex--;
                if (ScanIndex == -1) { break; }
                ScanDate = Convert.ToDateTime(Right.Table.Rows[ScanIndex]["Date"]);
            }
        }
    }

    static class HQDataRedundancy
    {
        #region Events
        // *** public static event EventHandler<LogEventArgs> LogMessage;
        #endregion
        static public void IdentifyActiveCSVs(Dictionary<string, string> ActiveCSV, Conjugacy main, Conjugacy red)
        {
            string[] filePaths = Directory.GetFiles(Constants.directoryCSV, "*.csv", SearchOption.AllDirectories);

            foreach (string pathToFile in filePaths)
            {
                string nameCSV = Support.Path2Name(pathToFile);

                if ((main.Lexicon.ContainsKey(nameCSV)) || (red.Lexicon.ContainsKey(nameCSV)))
                {
                    ActiveCSV.Add(nameCSV, "1");
                }
            }
        }

        static public void Execute(Dictionary<string, string> ActiveCSV, Conjugacy main, Conjugacy red)
        {
            var AvoidDuplicates = new Dictionary<string, string>();

            foreach (string name in ActiveCSV.Keys)
            {
                if (AvoidDuplicates.ContainsKey(name)) { continue; }

                if (main.Lexicon.ContainsKey(name))
                {
                    Conjugacy.IdentifyConjugate(name, ActiveCSV, AvoidDuplicates, main, red);
                }
                else if (red.Lexicon.ContainsKey(name))
                {
                    Conjugacy.IdentifyConjugate(name, ActiveCSV, AvoidDuplicates, red, main);
                }
                ProcessCSVpair(main, red);
            }

            AvoidDuplicates = null;
        }

        static public void ProcessCSVpair(Conjugacy main, Conjugacy red)
        {

            MeasurementLocation Main = new MeasurementLocation();
            Support.SetTableSchema(Main.Table);
            MeasurementLocation.ReadAndStoreCSV(main.CSVname, Main.Table);

            MeasurementLocation Red = new MeasurementLocation();
            Support.SetTableSchema(Red.Table);
            MeasurementLocation.ReadAndStoreCSV(red.CSVname, Red.Table);

            Main.SamplingRate = ( Convert.ToDouble(main.Lexicon[main.CSVname][1]) / 1000 ) / 60;  // convert msec to min
            Red.SamplingRate  = ( Convert.ToDouble(red.Lexicon[red.CSVname][1]) / 1000 ) / 60;

            MeasurementLocation.IsCounter = Convert.ToBoolean(Convert.ToInt16(main.Lexicon[main.CSVname][2]));

            Console.WriteLine("{0} --- {1}", main.CSVname, red.CSVname);

            if ((Main.Table.Rows.Count == 0) && (Red.Table.Rows.Count == 0))
            {
                // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DR-00000: MAIN  " + main.CSVname.ToString() + "  and Redundant  " + red.CSVname.ToString() + "  csv files are empty"));
            }
            else if ((Main.Table.Rows.Count == 0) && (Red.Table.Rows.Count != 0))
            {
                // If MAIN is not of counter-type, then copy stuff from RED to MAIN.
                // Otherwise, leave MAIN empty.
                if (!MeasurementLocation.IsCounter)
                {
                    Main.Table = Red.Table.Copy();
                    WriteNewCSV(main, Main);
                }
                else
                {
                    // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DR-PERIKL: MAIN  " + main.CSVname.ToString() + "csv file is empty and is of counter-type: Not allowed to copy from Redundant."));
                }
            }
            else if ((Main.Table.Rows.Count != 0) && (Red.Table.Rows.Count == 0))
            {
                // No action takes place: MAIN.csv remains as is.
            }
            else
            {
                // Ideal case where both MAIN and RED exist and are non-empty.
                InspectMissingValues(Main, Red);
                //InspectMissingValues(Red, Main); Copy the missing values from Red to Main. Uncomment if needed (and perform subsequent changes appropriately).
                InspectRogueValues(Main, Red);
                
                if ( (!MeasurementLocation.IsCounter) || (Main.Candidates.Count == 0) )
                {
                    WriteNewCSV(main, Main);
                }
                else
                {
                    Dictionary<int, double> baseRightValues = new Dictionary<int, double>();
                    GetBaseCounterValues(Main, Red, baseRightValues);
                    WriteNewCounterCSV(main, Main, baseRightValues);
                }
            }
        }

        static public void InspectMissingValues(MeasurementLocation Left, MeasurementLocation Right)
        {
            // On names "Left" and "Right":
            // Note that in ProcessCSVpair() we can call InspectMissingValues()
            // either as InspectMissingValues(Main, Red) or as InspectMissingValues(Red, Main).
            // In the former case we attempt to fill Main with values from Red (Main on the "Left" side and Red on the"Right" side), 
            // whereas in the latter case we do the opposite (Red on the "Left" side and Main on the"Right" side). 

            Right.Index = 0;

            for (Left.Index = 0; Left.Index <= Left.Table.Rows.Count; Left.Index++)
            {
                MainAlgorithm.SetLeftDate(Left);

                if (!MainAlgorithm.MissingValuesExist(Left)) { continue; }

                MainAlgorithm.SetRightDate(Left, Right);

                if (!MainAlgorithm.ProceedToSearch(Left, Right)) { continue; }

                MainAlgorithm.ScanRightForMissingValues(Left, Right);
            }
        }
        static public void InspectRogueValues(MeasurementLocation Left, MeasurementLocation Right)
        {

            Right.Index = 0;
            double Left_last_nonRogue_value = double.MinValue;

            for (Left.Index = 0; Left.Index <= Left.Table.Rows.Count-1; Left.Index++)
            {
                Left.CurrValue = Convert.ToDouble(Left.Table.Rows[Left.Index]["Measurement"]);

                if (Left.CurrValue == Constants.rogueReset)
                {
                    Left.CurrDate = Convert.ToDateTime(Left.Table.Rows[Left.Index]["Date"]);

                    // We need to check the Right value with a Right date as close as possible 
                    // to Left.CurrDate. Get the Right date immediately before Left.CurrDate:

                    MainAlgorithm.SetRightDate(Left, Right);

                    // Also need to get the Right Date immediately after Left.CurrDate
                    // and compare the two durations, i.e.,
                    // earlyDuration = duration between Left.CurrDate and RightDate immediately before Left.CurrDate 
                    // versus
                    // lateDuration  = duration between Left.CurrDate and RightDate immediately after Left.CurrDate:

                    if (Right.Index + 1 <= Right.Table.Rows.Count - 1)
                    {
                        TimeSpan earlyDuration = (Left.CurrDate - Right.CurrDate).Duration();

                        DateTime RightDummy = Convert.ToDateTime(Right.Table.Rows[Right.Index + 1]["Date"]);

                        TimeSpan lateDuration = (Left.CurrDate - RightDummy).Duration();

                        if (lateDuration < earlyDuration)
                        {
                            Right.Index++;
                            Right.CurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]); // = RightDummy
                        }
                    }

                    Right.CurrValue = Convert.ToDouble(Right.Table.Rows[Right.Index]["Measurement"]);

                    // We got the Right date which is as close as possible to Left.CurrDate
                    // and set it to Right.CurrDate as well as its corresponding value Right.CurrValue.
                    // However, we still need to ensure that Left.CurrDate and Right.CurrDate are close
                    // enough relative to some time unit, say, twice Left's sampling rate:
                    // (Remember that MeasurementLocation.SamplingRate is measured in minutes)
                    
                    if ( ((Left.CurrDate - Right.CurrDate).Duration()).TotalMinutes < Constants.confidence * Left.SamplingRate )
                    {
                        if (Right.CurrValue == Constants.rogueReset)
                        {
                            // In this case both Main and Red have "0",
                            // which (very likely) means that the corresponding ship's mechanism was idle at
                            // that time. "0" is thus an actual measurement and not a rogue value.
                            // Hence do nothing, keep everything as is.
                        }
                        else
                        {
                            if (!MeasurementLocation.IsCounter)
                            {
                                // Most likely, Left's "0" value is a rogue value, hence copy the actual value from Right.
                                Left.Table.Rows[Left.Index]["Measurement"] = Right.CurrValue;
                            }
                            else
                            {
                                // Need to compute
                                // " Left_last_nonRogue_value + Right.CurrValue - Right.PrevValue "
                                // We don't have Right.PrevValue. We could simply set Right.PrevValue = Right.Table.Rows[Right.Index - 1]["Measurement"].
                                // However, we have to ensure that Right.PrevValue is not a rogue value.
                                // We thus Right.Index-- until we find a Right non-rogue value.

                                int r = Right.Index;

                                if ((r > 0) && (Left_last_nonRogue_value != double.MinValue))
                                {
                                    while (r > 0)
                                    {
                                        r--;
                                        Right.PrevValue = Convert.ToDouble(Right.Table.Rows[r]["Measurement"]);
                                        if (Right.PrevValue != Constants.rogueReset)
                                        {
                                            Left.Table.Rows[Left.Index]["Measurement"] = Left_last_nonRogue_value + Right.CurrValue - Right.PrevValue;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    // No access to previous non-rogue values => cannot compute counter.
                                    // Keep Left.CurrValue (= 0) as is.
                                }
                            }
                        }
                    }
                    else
                    {
                        // They are too far apart, no safe decision can be made on Left's possibly rogue value.
                        // Keep Left.CurrValue (= 0) as is.
                    }
                }
                else
                {
                    Left_last_nonRogue_value = Left.CurrValue;
                }
            }
        }
        static public void WriteNewCSV(Conjugacy conj, MeasurementLocation ml)
        {
            try
            {
                File.Delete(Support.Name2Path(conj.CSVname));
                Thread.Sleep(Constants.sleepTime);
            }
            catch (Exception ex)
            {
                // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DR-00001: Cannot delete the CSV file   " + conj.CSVname.ToString() + "   - " + ex.Message));
            }

            try
            {
                using (StreamWriter file = new StreamWriter(Support.Name2Path(conj.CSVname)))
                {
                    for (int j = 0; j <= ml.Table.Rows.Count; j++)
                    {
                        if (ml.Candidates.ContainsKey(j))
                        {
                            for (int k = ml.Candidates[j].Rows.Count - 1; k >= 0; k--)
                            {
                                double CandidateMeasurement = Convert.ToDouble(ml.Candidates[j].Rows[k]["Measurement"]);

                                double LeftMeasurement;
                                if (j != ml.Table.Rows.Count)
                                {
                                    LeftMeasurement = Convert.ToDouble(ml.Table.Rows[j]["Measurement"]);
                                }
                                else
                                {
                                    LeftMeasurement = Convert.ToDouble(ml.Table.Rows[j-1]["Measurement"]);
                                }

                                if ( (LeftMeasurement != Constants.rogueReset) && (CandidateMeasurement == Constants.rogueReset)) { continue;  }

                                string date = (Convert.ToDateTime(ml.Candidates[j].Rows[k]["Date"])).ToString(Constants.dateFormat, CultureInfo.InvariantCulture);

                                file.WriteLine("{0},{1}", date, CandidateMeasurement.ToString());
                            }
                        }

                        if (j == ml.Table.Rows.Count) { return; }
                        Support.WriteLine(file, ml.Table, j);
                    }
                }
            }
            catch (Exception ex)
            {
                // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DR-00002: Something went wrong while writing to the new CSV   " + conj.CSVname.ToString() + "   - " + ex.Message));
            }
        }
        static public void GetBaseCounterValues(MeasurementLocation Left, MeasurementLocation Right, Dictionary<int, double> baseRightValues)
        {
            for (int j = 1; j <= Left.Table.Rows.Count; j++)
            {
                if (Left.Candidates.ContainsKey(j))
                {
                    Left.CurrDate = Convert.ToDateTime(Left.Table.Rows[j-1]["Date"]);
                    // Among all (Right) candidate dates, this Right date is the closest to Left.CurrDate:
                    int n = Left.Candidates[j].Rows.Count - 1;
                    Right.CurrDate = Convert.ToDateTime(Left.Candidates[j].Rows[n]["Date"]);
                    Right.Index = (int)Left.Candidates[j].Rows[n]["ConjugateIndex"];
                    // We want to find the Right date that is as close as possible to Left.CurrDate.
                    // It is definitely at an index of at most Right.Index. We thus have to scan backwards:

                    if (Right.Index != 0)
                    {
                        Right.PrevDate = Convert.ToDateTime(Right.Table.Rows[Right.Index - 1]["Date"]);
                        TimeSpan EarlierDuration = (Right.PrevDate - Left.CurrDate).Duration();
                        TimeSpan CurrDuration = (Right.CurrDate - Left.CurrDate).Duration();

                        if (Right.Index > 1)
                        {
                            while ((EarlierDuration <= CurrDuration) && (Right.Index > 1))
                            {
                                Right.Index--;
                                Right.CurrDate = Right.PrevDate;
                                Right.PrevDate = Convert.ToDateTime(Right.Table.Rows[Right.Index - 1]["Date"]);

                                EarlierDuration = (Right.PrevDate - Left.CurrDate).Duration();
                                CurrDuration = (Right.CurrDate - Left.CurrDate).Duration();
                            }
                        }

                        if (Right.Index == 1)
                        {
                            Right.Index = (EarlierDuration <= CurrDuration) ? 0 : 1;
                        }
                    }
                    
                    double measurement = Convert.ToDouble(Right.Table.Rows[Right.Index]["Measurement"]);
                    baseRightValues.Add(j-1, measurement);
                }
            }
        }
        static public void WriteNewCounterCSV(Conjugacy left, MeasurementLocation Left, Dictionary<int, double> baseRightValues)
        {
            try
            {
                File.Delete(Support.Name2Path(left.CSVname));
                Thread.Sleep(Constants.sleepTime);
            }
            catch (Exception ex)
            {
                // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DR-00001: Cannot delete the CSV file   " + left.CSVname.ToString() + "   - " + ex.Message));
            }

            try
            {
                using (StreamWriter file = new StreamWriter(Support.Name2Path(left.CSVname)))
                {
                    for (Left.Index = 0; Left.Index <= Left.Table.Rows.Count; Left.Index++)
                    {
                        if ( (Left.Index > 0) && (Left.Candidates.ContainsKey(Left.Index)) )
                        {
                            for (int k = Left.Candidates[Left.Index].Rows.Count - 1; k >= 0; k--)
                            {
                                Support.WriteCounterLine(file, Left, baseRightValues, k);
                            }
                        }

                        if (Left.Index == Left.Table.Rows.Count) { return; }
                        Support.WriteLine(file, Left.Table, Left.Index);
                    }
                }
            }
            catch (Exception ex)
            {
                // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DR-00002: Something went wrong while writing to the new CSV   " + left.CSVname.ToString() + "   - " + ex.Message));
            }
        }

    }

    //static class Print
    //{
    //    static public void printDict(Dictionary<string, string> D)
    //    {
    //        foreach (KeyValuePair<string, string> kvp in D)
    //        {
    //            Console.Write("|{0,-25}|", kvp.Key);
    //            Console.Write("|{0,-25}|", kvp.Value);
    //            Console.WriteLine();
    //        }
    //        Console.ReadLine();
    //    }

    //    static public void printDT(DataTable dt)
    //    {
    //        foreach (DataRow row in dt.Rows)
    //        {
    //            string[] arr = new string[2]; int i = 0;
    //            foreach (DataColumn col in dt.Columns) { arr[i] = row[col].ToString(); i++; }
    //            Console.WriteLine("{0} -- {1}", arr[0], arr[1]);
    //        }
    //        Console.ReadLine();
    //    }

    //    static public void printL(Dictionary<int, DataTable> list)
    //    {
    //        Console.WriteLine("\n~~~~~~~~~~~ Candidates~~~~~~~~~~~\n");
    //            foreach (KeyValuePair<int, DataTable> kvp in list)
    //            {
    //                Console.WriteLine("\t{0}", kvp.Key);
    //                    foreach (DataRow row in kvp.Value.Rows)
    //                    {
    //                        string[] arr = new string[2];
    //                        int s = 0;
    //                        foreach (DataColumn col in kvp.Value.Columns) { arr[s] = row[col].ToString(); s++; }
    //                        Console.Write("|{0} --- {1}|\n", arr[0], arr[1]);
    //                    }
    //                    Console.WriteLine();
    //            }
    //            Console.ReadLine();
    //    }
    //}

    static public class Service
    {
        static public void Moo()
        {

            Conjugacy main = new Conjugacy();
            Conjugacy red = new Conjugacy();

            DataAccess.ReadLexiconsInMemory(main.Lexicon, red.Lexicon);

            if (Conjugacy.ActiveCSV.Count > 0)
            {
                Conjugacy.ActiveCSV.Clear();
            }
            HQDataRedundancy.IdentifyActiveCSVs(Conjugacy.ActiveCSV, main, red);
            HQDataRedundancy.Execute(Conjugacy.ActiveCSV, main, red);
        }
    }
}
