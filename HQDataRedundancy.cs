﻿using System.Data;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;
using System.Threading;
using System.Globalization;

namespace HQDataRedundancy

{
    static class Constants
    {
        // *** public static string directoryCSV;
        public const int sleepTime = 10; // 1000 msec = 1 sec waiting time.
        public const string dateFormat = "dd/MM/yyyy HH:mm:ss.fff";
        public const double rogueDefunct1 = -999; // Rogue data value when measurement device sends no signal.  
        public const double rogueDefunct2 = -9999999; // Rogue data value when measurement device sends no signal.  
        public const double rogueReset = 0;  // Rogue data value when measurement device resets its operation.
        public const double loose = 0.9; // Relaxing the search space when investigating missing values. 0 < loose <= 1, only. Appropriate choice is 0.9
        public const int confidence = 2; // Extending the search space when investigating reset values. Positive integer (could also be double). Appropriate choice is 2 or 3.
    }

    static class Support
    {
        static public void WriteLine(StreamWriter file, DataTable dt, int n)
        {
            double measurement = Convert.ToDouble(dt.Rows[n]["Measurement"], CultureInfo.InvariantCulture);

            string date = (Convert.ToDateTime(dt.Rows[n]["Date"])).ToString(Constants.dateFormat, CultureInfo.InvariantCulture);

            file.WriteLine("{0},{1}", date, measurement.ToString());
        }

        static public void SetTableSchema(DataTable dt)
        {
            dt.Columns.Add("Date", typeof(DateTime));
            dt.Columns.Add("Measurement", typeof(double));
        }

        static public void GetClosestRightIndexToAGivenLeftDate_ScanBackwards(DateTime leftCurrDate, MeasurementLocation Right)
        {
            // We scan Right.Index backwards until we find a Right date, say D,
            // such that (leftCurrDate - D).Duration() is as small as possible.
            // The Right Index that corresponds to D is the output of this process.
            // Note that Right.Index will remain unchanged if no such D is found.

            if (Right.Index == 0) { return; }

            DateTime DummyRightCurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
            DateTime DummyRightPrevDate = Convert.ToDateTime(Right.Table.Rows[Right.Index - 1]["Date"]);
            TimeSpan EarlierDuration = (DummyRightPrevDate - leftCurrDate).Duration();
            TimeSpan CurrDuration = (DummyRightCurrDate - leftCurrDate).Duration();

            if (Right.Index > 1)
            {
                while ((EarlierDuration <= CurrDuration) && (Right.Index > 1))
                {
                    Right.Index--;
                    DummyRightCurrDate = DummyRightPrevDate;
                    DummyRightPrevDate = Convert.ToDateTime(Right.Table.Rows[Right.Index - 1]["Date"]);

                    EarlierDuration = (DummyRightPrevDate - leftCurrDate).Duration();
                    CurrDuration = (DummyRightCurrDate - leftCurrDate).Duration();
                }
            }

            if (Right.Index == 1)
            {
                Right.Index = (EarlierDuration <= CurrDuration) ? 0 : 1;
            }
        }

        static public void GetClosestRightIndexToAGivenLeftDate_ScanForwards(DateTime leftCurrDate, MeasurementLocation Right)
        {
            // We scan Right.Index forwards until we find a Right date, say D,
            // such that (leftCurrDate - D).Duration() is as small as possible.
            // The Right Index that corresponds to D is the output of this process.
            // Note that Right.Index will remain unchanged if no such D is found.

            if (Right.Index == Right.Table.Rows.Count - 1) { return; }

            DateTime DummyRightCurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index + 1]["Date"]);
            DateTime DummyRightPrevDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
            TimeSpan EarlierDuration = (DummyRightPrevDate - leftCurrDate).Duration();
            TimeSpan CurrDuration = (DummyRightCurrDate - leftCurrDate).Duration();

            int N = Right.Table.Rows.Count - 1;

            if (Right.Index < N - 1)
            {
                while ((CurrDuration <= EarlierDuration) && (Right.Index < N - 1))
                {
                    Right.Index++;
                    DummyRightPrevDate = DummyRightCurrDate;
                    DummyRightCurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index + 1]["Date"]);

                    EarlierDuration = (DummyRightPrevDate - leftCurrDate).Duration();
                    CurrDuration = (DummyRightCurrDate - leftCurrDate).Duration();
                }
            }

            if (Right.Index == N - 1)
            {
                Right.Index = (CurrDuration <= EarlierDuration) ? N : N - 1;
            }
        }

        static public string Name2Path(string name)
        {
            string[] quaxinfo = Regex.Split(name, "_");
            return HQDataRedundancyBackbone.directoryCSV + "Quax_" + quaxinfo[0] + "_ID_" + quaxinfo[1] + ".csv";
        }

        static public string Path2Name(string path)
        {
            string dummy = path.Replace(".csv", "");
            dummy = dummy.Replace(HQDataRedundancyBackbone.directoryCSV + "Quax_", "");
            string[] quaxinfo = Regex.Split(dummy, "_ID_");
            return quaxinfo[0] + "_" + quaxinfo[1];
        }
    }

    public class Conjugacy
    {

        #region Events
        // *** public static event EventHandler<LogEventArgs> LogMessage;
        #endregion

        /// <summary>
        /// This class is responsible for handling MAIN and RED CSVs, but NOT their content.
        /// </summary>

        //  Lexicon[LEFT][0] = RIGHT.
        //  Lexicon[LEFT][1] = samplingRate_LEFT
        //  Lexicon[LEFT][2] = is_counter
        public Dictionary<string, List<string>> Lexicon = new Dictionary<string, List<string>>();
        public string CSVname { get; set; }

        // Holds the names of all CSVs in directoryCSV. ActiveCSV[name] = "1" (value is always "1").
        public static Dictionary<string, string> ActiveCSV = new Dictionary<string, string>();

        static public void IdentifyConjugate(string name, Dictionary<string, string> ActiveCSV, Dictionary<string, string> AvoidDuplicates, Conjugacy left, Conjugacy right)
        {
            left.CSVname = name;
            right.CSVname = left.Lexicon[name][0];

            if (ActiveCSV.ContainsKey(right.CSVname))
            {
                // If conjugate csv right.CSVname exists in ActiveCSV then simply update AvoidDuplicates
                // (so that when right.name appears later when reading the directoryCSV, it won't be processed again).
                AvoidDuplicates.Add(right.CSVname, "1");
            }
            else
            {
                try
                {
                    // Otherwise, right.CSVname does not exist at all in the directoryCSV: 
                    // Create an empty csv file with the name right.CSVname anyway.
                    // This step only eases up the execution of subsequent steps. 
                    File.Create(Support.Name2Path(right.CSVname)).Dispose();
                    // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.MESSAGE, "CONJ-00000: The .csv file" + "  " + right.CSVname.ToString() + "  " + "was created, because it did not exist"));

                }
                catch (Exception ex)
                {
                    // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "CONJ-00000: Cannot create CSV file - " + ex.Message));
                }
            }
        }
    }

    public class MeasurementLocation
    {
        /// <summary>
        /// This class is responsible for handling the content of MAIN and RED CSVs.
        /// </summary>
        /// 

        //
        // Inroduction:
        // This service deals with pairs of csvs, which hold similar information and are 
        // chronologically synchronized. Whatever their actual names are (it was initially
        // designed for "MAIN" and "RED" csvs), we always compare two such csvs which
        // we term "LEFT" and "RIGHT".
        // A MeasurementLocation (ML) object holds the relevant data for each such csv.
        // 

        //
        // We store csv entries of a ML into a DataTable:
        //

        public DataTable Table = new DataTable();

        //
        // The work of this service is done by considering a pair
        // of Left csv lines and comparing it with an APPROPRIATE pair
        // of Right csv lines, and we do that for all consecutive Left csv line pairs.
        // This is performed across all tasks (with some some variations), from "autofill"
        // to deciding whether some csv values are "good" or "bad".
        //
        // In order to do that, we need to store the pair of Left csv data, i.e.,
        // Date & Value of line number i-1, which are stored in PrevDate and PrevValue, repsectively, and
        // Date & Value of line number i, which are stored in CurrDate and CurrValue, repsectively.
        //
        // Right objects also have the same format, but it is important to note that
        // Prev and Curr, may not necessarily refer to consecutive Right csv lines.
        // As implied from above, we scan Left's Table from start to finish and we
        // consider consecutive Left lines; for each such pair of consecutive lines, we 
        // seek the most appropriate pair of Right lines. "Appropriate" here depends on
        // the task in hand. For example for Left's Prev line i-1, its most "appropriate" Right line  
        // may be Right line number j, but for Left's Curr line i, its most "appropriate" Right line  
        // may be Right line number j+3.
        //
        // For any ML object, we use an "Index" to navigate up and down its Table.
        //

        public int Index { get; set; }

        public DateTime CurrDate { get; set; }

        public DateTime PrevDate { get; set; }

        public double CurrValue { get; set; }

        public double PrevValue { get; set; }

        // Sampling rate of ML measured in *** MINUTES ***.
        public double SamplingRate { get; set; }

        // The following static variable informs us whether a csv pair under consideartion
        // is of counter type or not. 
        // "Static" because if Left is of counter-type, then so is Right and vice versa.
        // A note on this static variable: It is independent of the lifetime of the 
        // ML objects. Once IsCounter is switched to "true"
        // it will remain so until we decide to switch it back to "false".

        public static bool IsCounter = false;

        // The following dictionary is used to store the missing entries of a given csv 
        // which are taken from its conjugate csv. A Left.Index (key) points to a 
        // DataTable of missing entries which are discovered in Right.Table. 

        public Dictionary<int, DataTable> Missing = new Dictionary<int, DataTable>();

        static public void ReadAndStoreCSV(string name, DataTable mlTable)
        {
            string pathToFile = Support.Name2Path(name);

            using (StreamReader sreader = File.OpenText(pathToFile))
            {
                string line = String.Empty;
                while ((line = sreader.ReadLine()) != null)
                {
                    string[] data = Regex.Split(line, ",");
                    DateTime date = DateTime.ParseExact(data[0], Constants.dateFormat, CultureInfo.InvariantCulture);
                    double measurement = Convert.ToDouble(data[1]);
                    if ( (measurement == Constants.rogueDefunct1) || (measurement == Constants.rogueDefunct2) )
                    {
                        continue;
                    }
                    mlTable.Rows.Add(date, measurement);
                }
            }
        }
    }

    public class HQDataRedundancyAlgorithms
    {
        public static class MissingValuesAlgorithm
        {
            static public void SetLeftDate(MeasurementLocation Left)
            {
                // Set the time interval for Left:
                if (Left.Index == 0)
                {
                    // Start: (-infinity, time_initial) 
                    Left.PrevDate = DateTime.MinValue;
                    Left.CurrDate = Convert.ToDateTime(Left.Table.Rows[Left.Index]["Date"]);
                }
                else if (Left.Index == Left.Table.Rows.Count)
                {
                    // End: (time_final, +infinity)
                    Left.PrevDate = Convert.ToDateTime(Left.Table.Rows[Left.Index - 1]["Date"]);
                    Left.CurrDate = DateTime.MaxValue;
                }
                else
                {
                    // (time_{k-1}, time_k)
                    Left.PrevDate = Convert.ToDateTime(Left.Table.Rows[Left.Index - 1]["Date"]);
                    Left.CurrDate = Convert.ToDateTime(Left.Table.Rows[Left.Index]["Date"]);
                }
            }

            static public bool MissingValuesExist(MeasurementLocation Left)
            {
                if ((Left.CurrDate - Left.PrevDate).TotalMinutes < 2 * Left.SamplingRate)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            static public void SetRightDate(MeasurementLocation Left, MeasurementLocation Right)
            {
                // Set Right.CurrDate by finding appropriate index Right.Index such that 
                // Left.CurrDate > Right.CurrDate (at that Right.Index) 
                // Left.CurrDate < Right.CurrDate (at Right.Index + 1).
                // In other words, find index Right.Index such that its corresponding  
                // Right.CurrDate is a date immediately before date Left.CurrDate. 

                Right.CurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
                while (Right.CurrDate < Left.CurrDate)
                {
                    if (Right.Index == Right.Table.Rows.Count - 1) { break; }
                    Right.Index++;
                    Right.CurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
                    if (Right.CurrDate >= Left.CurrDate)
                    {
                        Right.Index--;
                        Right.CurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
                        break;
                    }
                }
            }

            static public bool ProceedToSearch(MeasurementLocation Left, MeasurementLocation Right)
            {
                if ((Left.PrevDate >= Right.CurrDate) || (Right.CurrDate >= Left.CurrDate))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            static public void ScanRightForMissingValues(MeasurementLocation Left, MeasurementLocation Right)
            {
                DateTime ScanDate = Right.CurrDate;
                int ScanIndex = Right.Index;
                bool isInitialized = false;

                // Starting from Right.CurrDate, scan Right backwards until you exceed Left.PrevDate.
                // In other words, stay within the time interval as in ProceedToSearch() and compare
                // Left and Right dates. 
                // 
                // If a ScanDate is distant enough from Left.CurrDate and Left.PrevDate, then the line
                // in Right that corresponds to ScanDate is added to Left's candidate list.
                //
                // The loose factor (0 < loose <= 1) relaxes the search space for an appropriate (missing) Right date. 

                while (ScanDate > Left.PrevDate)
                {
                    bool satisfy_lower_bound = (ScanDate - Left.PrevDate).TotalMinutes > Constants.loose * Left.SamplingRate;
                    bool satisfy_upper_bound = (Left.CurrDate - ScanDate).TotalMinutes > Constants.loose * Left.SamplingRate;

                    if (satisfy_lower_bound && satisfy_upper_bound)
                    {
                        if (!isInitialized)
                        {
                            Left.Missing.Add(Left.Index, new DataTable());
                            Support.SetTableSchema(Left.Missing[Left.Index]);
                            Left.Missing[Left.Index].Columns.Add("ConjugateIndex", typeof(int));
                            isInitialized = true;
                        }

                        double measurement = Convert.ToDouble(Right.Table.Rows[ScanIndex]["Measurement"], CultureInfo.InvariantCulture);

                        Left.Missing[Left.Index].Rows.Add(ScanDate, measurement, ScanIndex);
                    }

                    ScanIndex--;
                    if (ScanIndex == -1) { break; }
                    ScanDate = Convert.ToDateTime(Right.Table.Rows[ScanIndex]["Date"]);
                }
            }
        }

        public static class OwnResetValuesAlgorithm
        {
            static public void GetClosestRightDateToAGivenLeftDate(MeasurementLocation Left, MeasurementLocation Right)
            {
                Support.GetClosestRightIndexToAGivenLeftDate_ScanForwards(Left.CurrDate, Right);
                Right.CurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
            }

            static public bool ProceedToReplacement(MeasurementLocation Left, MeasurementLocation Right)
            {
                double TimeDiff = ((Left.CurrDate - Right.CurrDate).Duration()).TotalMinutes;

                if (TimeDiff >= Constants.confidence * Left.SamplingRate)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            static public void DecideResetValueReplacement(MeasurementLocation Left, MeasurementLocation Right)
            {
                Right.CurrValue = Convert.ToDouble(Right.Table.Rows[Right.Index]["Measurement"], CultureInfo.InvariantCulture);

                if (Right.CurrValue == Constants.rogueReset)
                {
                    // In this case both Main and Red have "0", which (very likely) means
                    // that the corresponding ship's mechanism was idle at that time.                   
                    // "0" is thus an actual measurement and not a rogue value.
                    // Hence do nothing, keep everything as is.
                }
                else
                {
                    // Most likely, Left's "0" value is a rogue value, hence copy the actual value from Right.
                    Left.Table.Rows[Left.Index]["Measurement"] = Right.CurrValue;
                }
            }

        }

        public static class CopiedResetValuesAlgorithm
        {

            static public void Execute(MeasurementLocation Left, MeasurementLocation Right, List<bool> isRowFromRight, List<int> BlockOfCopiedResetIndices, Dictionary<int, bool> RejectRow)
            {
                //
                // The following table illustrates the case under consideration:
                // A dashed line separates lines in the Left and Right tables.
                // We have copied over the block of reset values (0) form Right to Left
                // We want to verify whether this insertion into Left is correct.
                // 
                // We do that by exploring the boundaries of the block, for both Left
                // and Right. 
                // LeftBound.Prev is the row immediately before the block
                // in Left and LeftBound.Curr is the row immediately after the block
                // in Left. Similarly for RightBound.
                //
                // 
                //        ---------------------------------------     | 
                //         LeftBound.Prev   |    RightBound.Prev      |
                //        ---------------------------------------     |
                //               0          |         0               |
                //        ---------------------------------------     |
                //               .          |         .               |
                //               .          |         .               |
                //               .          |         .               |
                //        ----------------------------------------    |
                //               0          |         0               |
                //        ----------------------------------------    |
                //         LeftBound.Curr   |    RightBound.Curr      |
                //        ----------------------------------------    |  TIME
                //                                                    V
                // The block is rejected if:
                // * LeftBound.PrevDate and RightBound.PrevDate are far apart, or
                // * LeftBound.CurrDate and RightBound.CurrDate are far apart, or
                // * LeftBound.PrevValue != 0  &&  RightBound.PrevValue == 0, or
                // * LeftBound.PrevValue != 0  &&  RightBound.PrevValue == 0.
                //
                // We accept the block whenever LeftBound.PrevValue = 0 or LeftBound.CurrValue = 0.
                // First of all, LeftBound.CurrValue = 0 means that this row was excluded from the block, because it
                // has not been copied over from Right, i.e., it has already existed in Left. 
                // This implies that the line that has LeftBound.CurrValue = 0
                // was already examined in InspectOwnResetValues(). There, it was decide that LeftBound.CurrValue = 0
                // should stay as is. This further implies that the block of zeros should not be rejected, as these
                // reset values are most likely actual reset values. Similarly for LeftBound.PrevValue = 0.
                //

                //
                // 1. Get LeftBound:
                //

                MeasurementLocation LeftBound = new MeasurementLocation();

                int[] LeftBoundIndices = GetLeftBound(Left, LeftBound, BlockOfCopiedResetIndices);

                //
                // 2. Check whether LeftBound has (non-copied) reset values
                //    If true, then we probably want to keep the block:

                if (DecideLeftBoundResetValue(LeftBound, LeftBoundIndices)) { return; }

                //
                // 3. Check if LeftBound is copied from Right (obviously non-reset copies, for if otherwise
                //    they would have been part of the block we are investigating).
                //

                bool[] IsLeftBoundCopied = FindIsLeftBoundCopied(isRowFromRight, LeftBoundIndices);

                if (DecideLeftBoundCopied(IsLeftBoundCopied)) { return; }

                //
                // 4. Search for RightBound:
                //

                MeasurementLocation RightBound = new MeasurementLocation();

                int[] RightBoundIndices = GetRightBound(Right, RightBound, LeftBound, LeftBoundIndices, IsLeftBoundCopied);

                //
                // 5. Compare LeftBound and RightBound:
                // 

                bool[] TestKeepBlock = CompareLeftAndRightBounds(Left, LeftBound, RightBound, RightBoundIndices);

                //
                // 6. Final decision:
                //

                if (DecideKeepBlock(TestKeepBlock)) { return; }

                //
                // 7. Failed tests -- reject the whole block:
                //

                int f = BlockOfCopiedResetIndices[0];
                int l = BlockOfCopiedResetIndices[BlockOfCopiedResetIndices.Count - 1];

                for (int j = f; j <= l; j++) { RejectRow[j] = true; }

            }

            static public int[] GetLeftBound(MeasurementLocation Left, MeasurementLocation LeftBound, List<int> BlockOfCopiedResetIndices)
            {
                int LeftBoundPrevIndex = -1; // inf index -1 means that we are right before the beginning of the table

                int f = BlockOfCopiedResetIndices[0]; // first Left index in the block

                if (f != 0)
                {
                    LeftBound.PrevValue = Convert.ToDouble(Left.Table.Rows[f - 1]["Measurement"], CultureInfo.InvariantCulture);
                    LeftBound.PrevDate = Convert.ToDateTime(Left.Table.Rows[f - 1]["Date"]);
                    LeftBoundPrevIndex = f - 1;
                }

                int LeftBoundCurrIndex = -1; // sup index -1 means that we are right after the bottom of the table

                int l = BlockOfCopiedResetIndices[BlockOfCopiedResetIndices.Count - 1]; // last Left index in the block
                int N = Left.Table.Rows.Count - 1;

                if (l != N)
                {
                    LeftBound.CurrValue = Convert.ToDouble(Left.Table.Rows[l + 1]["Measurement"], CultureInfo.InvariantCulture);
                    LeftBound.CurrDate = Convert.ToDateTime(Left.Table.Rows[l + 1]["Date"]);
                    LeftBoundCurrIndex = l + 1;
                }

                return new int[] { LeftBoundPrevIndex, LeftBoundCurrIndex };
            }

            static public bool DecideLeftBoundResetValue(MeasurementLocation LeftBound, int[] LeftBoundIndices)
            {
                //
                // This is a quick test that can easily determine if the block should be rejected or not:
                //
                // * If both LeftBound.CurrVal and LeftBound.PrevVal are (non-copied) reset value, 
                //   then it may be a good idea not to reject the block.
                //   
                // * If one of LeftBoundIndices[0] and LeftBoundIndices[1] is -1, i.e., 
                //   if one side of the block is the boundary of the csv,
                //   and for the other we have LeftBound.*Val to be a (non-copied) reset value,
                //   then it may be a good idea not to reject the block.
                //
                //  Two remarks:
                //  
                //  1) We emphasize the "non-copied" part, for otherwise this reset value (if it exists)
                //  would have been part of the block!!
                //
                //  2) If LeftBound.CurrVal and/or LeftBound.PrevVal are/is (a) (non-copied) reset value(s), 
                //  then their/its correpsonding RightBound.CurrVal and/or RightBound.PrevVal are/is guaranteed
                //  to be also (a) reset value(s).
                //  We know that because the InspectOwnResetValues() tests told us so: Left compared its own
                //  (i.e., non-copied) reset values with the values of the closest rows in Right. 
                //  There, it was found that if (Left.Val = 0) & (Right.Val != 0), then Left.Val := Right.Val. 
                //  Hence, whenever we see here that Left.Val is a (non-copied) reset value, 
                //  we automatically know that its closest Right value is also a reset value.)
                //

                int prevInd = LeftBoundIndices[0];
                int currInd = LeftBoundIndices[1];

                double prevVal = LeftBound.PrevValue;
                double currVal = LeftBound.CurrValue;

                double r = Constants.rogueReset;

                if ((prevInd != -1) && (currInd != -1))
                {
                    if ((prevVal == r) && (currVal == r))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if ((prevInd != -1) && (currInd == -1))
                {
                    if (prevVal == r)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if ((prevInd == -1) && (currInd != -1))
                {
                    if (currVal == r)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    // ((prevInd == -1) && (currInd == -1))
                    // This actually never happens. 
                    // It means that the Main csv is empty, a case that 
                    // is being taken care of in ProcessCSVPair().
                    return true;
                }
            }

            static public bool[] FindIsLeftBoundCopied(List<bool> isRowFromRight, int[] LeftBoundIndices)
            {
                //
                // If LeftBound.Curr and LeftBound.Prev are also copies form Right,
                // then there is nothing to reject.
                //

                bool isPrevCopied = false;
                if (LeftBoundIndices[0] != -1)
                {
                    if (isRowFromRight[LeftBoundIndices[0]]) { isPrevCopied = true; }
                }

                bool isCurrCopied = false;
                if (LeftBoundIndices[1] != -1)
                {
                    if (isRowFromRight[LeftBoundIndices[1]]) { isCurrCopied = true; }
                }

                return new bool[] { isPrevCopied, isCurrCopied };

            }

            static public bool DecideLeftBoundCopied(bool[] IsLeftBoundCopied)
            {
                if ((IsLeftBoundCopied[0]) && (IsLeftBoundCopied[1]))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            static public int[] GetRightBound(MeasurementLocation Right, MeasurementLocation RightBound, MeasurementLocation LeftBound, int[] LeftBoundIndices, bool[] IsLeftBoundCopied)
            {
                int RightBoundPrevIndex = -1; // prev index -1 means that we are right before the beginning of the table

                if ((LeftBoundIndices[0] != -1) && (!IsLeftBoundCopied[0]))
                {
                    Support.GetClosestRightIndexToAGivenLeftDate_ScanForwards(LeftBound.PrevDate, Right);
                    RightBound.PrevValue = Convert.ToDouble(Right.Table.Rows[Right.Index]["Measurement"], CultureInfo.InvariantCulture);
                    RightBound.PrevDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
                    RightBoundPrevIndex = Right.Index;
                }

                int RightBoundCurrIndex = -1; // Curr index -1 means that we are right after the bottom of the table

                if ((LeftBoundIndices[1] != -1) && (!IsLeftBoundCopied[1]))
                {
                    Support.GetClosestRightIndexToAGivenLeftDate_ScanForwards(LeftBound.CurrDate, Right);
                    RightBound.CurrValue = Convert.ToDouble(Right.Table.Rows[Right.Index]["Measurement"], CultureInfo.InvariantCulture);
                    RightBound.CurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
                    RightBoundCurrIndex = Right.Index;
                }

                return new int[] { RightBoundPrevIndex, RightBoundCurrIndex };
            }

            static public bool[] CompareLeftAndRightBounds(MeasurementLocation Left, MeasurementLocation LeftBound, MeasurementLocation RightBound, int[] RightBoundIndices)
            {
                bool keepBlock_PrevTest = true;

                if (RightBoundIndices[0] != -1)
                {
                    double TimeDiff = ((LeftBound.PrevDate - RightBound.PrevDate).Duration()).TotalMinutes;

                    if (TimeDiff >= Constants.confidence * Left.SamplingRate)
                    {
                        // RightBound.Prev is too far apart from LeftBound.Prev and thus not trustworthy.
                        keepBlock_PrevTest = false;
                    }
                    else
                    {
                        Console.WriteLine("{0} ||| {1}", LeftBound.PrevValue, RightBound.PrevValue);

                        if ((LeftBound.PrevValue != Constants.rogueReset) && (RightBound.PrevValue == Constants.rogueReset))
                        {
                            // If LeftBound.PrevValue is not a reset value and its corresponding RightBound.PrevValue IS a reset value,
                            // then this means that the RightBound.PrevValue reset value is NOT trustworthy.
                            // Hence, don't keep the block of zeros, as most likely Right was malfunctioning at that stage.
                            keepBlock_PrevTest = false;
                        }
                    }
                }

                bool keepBlock_CurrTest = true;

                if (RightBoundIndices[1] != -1)
                {
                    double TimeDiff = ((LeftBound.CurrDate - RightBound.CurrDate).Duration()).TotalMinutes;

                    if (TimeDiff >= Constants.confidence * Left.SamplingRate)
                    {
                        keepBlock_CurrTest = false;
                    }
                    else
                    {
                        Console.WriteLine("{0} ||| {1}", LeftBound.CurrValue, RightBound.CurrValue);

                        if ((LeftBound.CurrValue != Constants.rogueReset) && (RightBound.CurrValue == Constants.rogueReset))
                        {
                            keepBlock_CurrTest = false;
                        }
                    }
                }

                return new bool[] { keepBlock_PrevTest, keepBlock_CurrTest };
            }

            static public bool DecideKeepBlock(bool[] TestKeepBlock)
            {
                if (TestKeepBlock[0] && TestKeepBlock[1])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static class UnchangedCounterValuesAlgorithm
        {
            static public void GetClosestRightToAGivenLeft(MeasurementLocation Left, MeasurementLocation Right)
            {
                if (Left.Index != 0)
                {
                    Support.GetClosestRightIndexToAGivenLeftDate_ScanForwards(Left.PrevDate, Right);
                    Right.PrevDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
                    Right.PrevValue = Convert.ToDouble(Right.Table.Rows[Right.Index]["Measurement"], CultureInfo.InvariantCulture);
                }

                Support.GetClosestRightIndexToAGivenLeftDate_ScanForwards(Left.CurrDate, Right);
                Right.CurrDate = Convert.ToDateTime(Right.Table.Rows[Right.Index]["Date"]);
                Right.CurrValue = Convert.ToDouble(Right.Table.Rows[Right.Index]["Measurement"], CultureInfo.InvariantCulture);
            }

            static public bool ProceedToReplacement(MeasurementLocation Left, MeasurementLocation Right)
            {
                double CurrTimeDiff = ((Left.CurrDate - Right.CurrDate).Duration()).TotalMinutes;
                double PrevTimeDiff = ((Left.PrevDate - Right.PrevDate).Duration()).TotalMinutes;

                bool CurrTest = (CurrTimeDiff >= Constants.confidence * Left.SamplingRate);
                bool PrevTest = (PrevTimeDiff >= Constants.confidence * Left.SamplingRate);

                if (CurrTest || PrevTest)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            static public void DecideUnchangedValueReplacement(MeasurementLocation Left, MeasurementLocation Right, ref double accumulation)
            {
                if (Right.CurrValue == Right.PrevValue)
                {
                    // In this case we have :
                    //      * Left's Prev & Curr values are unchanged.
                    //      * Right's values whose dates correspond to Left's Prev & Curr nearest dates are also unchanged.
                    // This (very likely) means that the corresponding ship's mechanism was idle at that time.                   
                    // In other words, the fact that Left's value has remained unchanged is not a sign of malfunction.
                    // Hence do nothing, keep everything as is.
                }
                else
                {
                    // Most likely, Left's Curr unchanged value (form its Prev value) means that Left was malfunctioning.
                    // This can be fixed by getting the counter increment Right.CurrValue - Right.PrevValue from Right
                    // and adding it to Left.CurrValue. 
                    // Hence, this quantity replaces the unchanged value in Left's table.

                    double increment = Right.CurrValue - Right.PrevValue;

                    Left.Table.Rows[Left.Index]["Measurement"] = Left.CurrValue + increment;

                    accumulation += increment;
                }
            }

        }
    }

    public class HQDataRedundancyBackbone
    {
        #region Events
        // *** public static event EventHandler<LogEventArgs> LogMessage;
        #endregion

        public static string directoryCSV;

        static public void SetExperimentDirectory(string testdir)
        {
            //string RootDirectory = "C:\\Users\\spyros.martzoukos\\Desktop\\HQdataRedundancyTEST\\HQDR_V2\\testHQ\\datasets\\" + Constants.TestDirectory;
            //string TestDirectory = testdir + "\\this_output\\";

            //string RootDirectory = "/home/spyros/prisma/hqdataredundancy/" + Constants.TestDirectory;
            //string TestDirectory = "test_" + experiment.ToString() + "/this_output/";

            //directoryCSV = RootDirectory + TestDirectory;

            directoryCSV = testdir + "\\this_output\\";
        }

        static public void IdentifyActiveCSVs(Dictionary<string, string> ActiveCSV, Conjugacy main, Conjugacy red)
        {
            string[] filePaths = Directory.GetFiles(HQDataRedundancyBackbone.directoryCSV, "*.csv", SearchOption.AllDirectories);

            foreach (string pathToFile in filePaths)
            {
                string nameCSV = Support.Path2Name(pathToFile);

                if ((main.Lexicon.ContainsKey(nameCSV)) || (red.Lexicon.ContainsKey(nameCSV)))
                {
                    ActiveCSV.Add(nameCSV, "1");
                }
            }
        }

        static public void Execute(Dictionary<string, string> ActiveCSV, Conjugacy main, Conjugacy red)
        {
            var AvoidDuplicates = new Dictionary<string, string>();

            foreach (string name in ActiveCSV.Keys)
            {
                if (AvoidDuplicates.ContainsKey(name)) { continue; }

                if (main.Lexicon.ContainsKey(name))
                {
                    Conjugacy.IdentifyConjugate(name, ActiveCSV, AvoidDuplicates, main, red);
                }
                else if (red.Lexicon.ContainsKey(name))
                {
                    Conjugacy.IdentifyConjugate(name, ActiveCSV, AvoidDuplicates, red, main);
                }
                ProcessCSVpair(main, red);
            }
        }

        static public void ProcessCSVpair(Conjugacy main, Conjugacy red)
        {
            MeasurementLocation Main = new MeasurementLocation();
            MeasurementLocation Red = new MeasurementLocation();
            PopulateConjugateDataObjects(main, red, Main, Red);

            Dictionary<int, bool> RejectRow = new Dictionary<int, bool>();

            if ((Main.Table.Rows.Count == 0) && (Red.Table.Rows.Count == 0))
            {
                // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DR-00000: MAIN  " + main.CSVname.ToString() + "  and Redundant  " + red.CSVname.ToString() + "  csv files are empty"));
            }
            else if ((Main.Table.Rows.Count == 0) && (Red.Table.Rows.Count != 0))
            {
                // If MAIN is not of counter-type, then copy stuff from RED to MAIN.
                // Otherwise, leave MAIN empty.
                if (!MeasurementLocation.IsCounter)
                {
                    Main.Table = Red.Table.Copy();
                    WriteNewCSV(main, Main, RejectRow);
                }
                else
                {
                    // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DR-PERIKL: MAIN  " + main.CSVname.ToString() + "csv file is empty and is of counter-type: Not allowed to copy from Redundant."));
                }
            }
            else if ((Main.Table.Rows.Count != 0) && (Red.Table.Rows.Count == 0))
            {
                // No action takes place: MAIN.csv remains as is.
            }
            else
            {
                // Ideal case where both MAIN and RED exist and are non-empty.
                MeasurementLocation NewMain = new MeasurementLocation();
                Support.SetTableSchema(NewMain.Table);
                NewMain.SamplingRate = Main.SamplingRate;

                HQDataRedundancyMainTasks.Execute(Main, Red, NewMain, RejectRow);

                WriteNewCSV(main, NewMain, RejectRow);
            }
        }

        static public void PopulateConjugateDataObjects(Conjugacy main, Conjugacy red, MeasurementLocation Main, MeasurementLocation Red)
        {
            Support.SetTableSchema(Main.Table);
            MeasurementLocation.ReadAndStoreCSV(main.CSVname, Main.Table);

            Support.SetTableSchema(Red.Table);
            MeasurementLocation.ReadAndStoreCSV(red.CSVname, Red.Table);

            Main.SamplingRate = (Convert.ToDouble(main.Lexicon[main.CSVname][1]) / 1000) / 60;  // convert msec to min
            Red.SamplingRate = (Convert.ToDouble(red.Lexicon[red.CSVname][1]) / 1000) / 60;

            MeasurementLocation.IsCounter = Convert.ToBoolean(Convert.ToInt16(main.Lexicon[main.CSVname][2]));

            Console.WriteLine(directoryCSV);
            Console.WriteLine("{0} --- {1}", main.CSVname, red.CSVname);
        }

        static public void WriteNewCSV(Conjugacy left, MeasurementLocation Left, Dictionary<int, bool> RejectRow)
        {
            try
            {
                File.Delete(Support.Name2Path(left.CSVname));
                Thread.Sleep(Constants.sleepTime);
            }
            catch (Exception ex)
            {
                // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DR-00001: Cannot delete the CSV file   " + left.CSVname.ToString() + "   - " + ex.Message));
            }

            try
            {
                using (StreamWriter file = new StreamWriter(Support.Name2Path(left.CSVname)))
                {
                    for (int i = 0; i < Left.Table.Rows.Count; i++)
                    {
                        if (RejectRow.ContainsKey(i)) { continue; }

                        Support.WriteLine(file, Left.Table, i);
                    }
                }
            }
            catch (Exception ex)
            {
                // *** if (LogMessage != null) LogMessage(null, new LogEventArgs(LogType.ERROR, "DR-00002: Something went wrong while writing to the new CSV   " + left.CSVname.ToString() + "   - " + ex.Message));
            }
        }
    }

    public class HQDataRedundancyMainTasks
    {

        static public void Execute(MeasurementLocation Main, MeasurementLocation Red, MeasurementLocation NewMain, Dictionary<int, bool> RejectRow)
        {
            List<bool> isRowFromConjugate = new List<bool>();

            if (!MeasurementLocation.IsCounter)
            {
                InspectMissingValues(Main, Red);

                ConstructNewTable(Main, NewMain, isRowFromConjugate);

                InspectOwnResetValues(NewMain, Red, isRowFromConjugate);

                InspectCopiedResetValues(NewMain, Red, isRowFromConjugate, RejectRow);
            }
            else
            {
                MakeIncreasingCounter(Red, 0);

                InspectMissingValues(Main, Red);

                Dictionary<int, double> BaseConjugateValues = new Dictionary<int, double>();
                GetBaseCounterValues(Main, Red, BaseConjugateValues);

                double negative_offset = ConstructNewCounterTable(Main, BaseConjugateValues, NewMain);

                MakeIncreasingCounter(NewMain, negative_offset);

                InspectUnchangedCounterValues(NewMain, Red);
            }
        }

        static public void InspectMissingValues(MeasurementLocation Left, MeasurementLocation Right)
        {
            // On names "Left" and "Right":
            // Note that in ProcessCSVpair() we can call InspectMissingValues()
            // either as InspectMissingValues(Main, Red) or as InspectMissingValues(Red, Main).
            // In the former case we attempt to fill Main with values from Red (Main on the "Left" side and Red on the"Right" side), 
            // whereas in the latter case we do the opposite (Red on the "Left" side and Main on the"Right" side). 

            Right.Index = 0;

            for (Left.Index = 0; Left.Index <= Left.Table.Rows.Count; Left.Index++)
            {
                HQDataRedundancyAlgorithms.MissingValuesAlgorithm.SetLeftDate(Left);

                if (!HQDataRedundancyAlgorithms.MissingValuesAlgorithm.MissingValuesExist(Left)) { continue; }

                HQDataRedundancyAlgorithms.MissingValuesAlgorithm.SetRightDate(Left, Right);

                if (!HQDataRedundancyAlgorithms.MissingValuesAlgorithm.ProceedToSearch(Left, Right)) { continue; }

                HQDataRedundancyAlgorithms.MissingValuesAlgorithm.ScanRightForMissingValues(Left, Right);
            }
        }

        static public void ConstructNewTable(MeasurementLocation Left, MeasurementLocation NewLeft, List<bool> isRowFromRight)
        {
            // Construct a new Left table that is the same as Left, but with the retrived 
            // Right rows included. This is the "autofill" operation.
            // List isRowFromRight stores whether a new Left index was taken from Right,
            // i.e., isRowFromRight[i] = true, if line number i (in the new Left)
            // was taken from right, and false otherwise.

            for (Left.Index = 0; Left.Index <= Left.Table.Rows.Count; Left.Index++)
            {
                DateTime date;
                double measurement;

                if (Left.Missing.ContainsKey(Left.Index))
                {
                    for (int k = Left.Missing[Left.Index].Rows.Count - 1; k >= 0; k--)
                    {
                        date = Convert.ToDateTime(Left.Missing[Left.Index].Rows[k]["Date"]);
                        measurement = Convert.ToDouble(Left.Missing[Left.Index].Rows[k]["Measurement"], CultureInfo.InvariantCulture);

                        NewLeft.Table.Rows.Add(date, measurement);
                        isRowFromRight.Add(true);
                    }
                }

                if (Left.Index == Left.Table.Rows.Count) { return; }

                date = Convert.ToDateTime(Left.Table.Rows[Left.Index]["Date"]);
                measurement = Convert.ToDouble(Left.Table.Rows[Left.Index]["Measurement"], CultureInfo.InvariantCulture);

                NewLeft.Table.Rows.Add(date, measurement);
                isRowFromRight.Add(false);
            }
        }

        static public void InspectOwnResetValues(MeasurementLocation Left, MeasurementLocation Right, List<bool> isRowFromRight)
        {
            //
            // We examine whether Left's reset values are actual signs of the mechanism's idleness, or 
            // signs of malfunction. Note that, in this method, we investigate only the 
            // reset values that Left has in its original csv, i.e., it has nothing to do
            // with the autofill operation.
            // The treatment is simple: If Left's reset value is not matched by a reset value in
            // Right within a reasonable time window, then Left's reset value is probably rogue.
            // It is thus replaced by that -- chronologically matched -- Right value:
            // The replacement value is the Right measuremet of the 
            // nearest possible time. If no timewise close comparisons can be made,
            // then Left's reset value remains as is.
            //

            Right.Index = 0;

            for (Left.Index = 0; Left.Index < Left.Table.Rows.Count; Left.Index++)
            {
                if (isRowFromRight[Left.Index]) { continue; }

                Left.CurrValue = Convert.ToDouble(Left.Table.Rows[Left.Index]["Measurement"], CultureInfo.InvariantCulture);

                if (Left.CurrValue != Constants.rogueReset) { continue; }

                Left.CurrDate = Convert.ToDateTime(Left.Table.Rows[Left.Index]["Date"]);

                HQDataRedundancyAlgorithms.OwnResetValuesAlgorithm.GetClosestRightDateToAGivenLeftDate(Left, Right);

                if (!HQDataRedundancyAlgorithms.OwnResetValuesAlgorithm.ProceedToReplacement(Left, Right)) { continue; }

                HQDataRedundancyAlgorithms.OwnResetValuesAlgorithm.DecideResetValueReplacement(Left, Right);
            }
        }

        static public void InspectCopiedResetValues(MeasurementLocation Left, MeasurementLocation Right, List<bool> isRowFromRight, Dictionary<int, bool> RejectRow)
        {
            //
            // We investigate the reset values that Left inherited/copied from Right.
            // If multiple consecutive such resets values can be found then 
            // the block of these reset values is examined.
            // See CopiedResetValuesAlgorithm.Execute() for more details.
            // Note that this examination is much different from InspectOwnResetValues():
            // In InspectOwnResetValues() we compare each Left's OWN reset value with 
            // the chronoligally closest value from Right. 
            // Here, we treat the case where Right has reset values
            // at timestamps when Left did not have any values at all. 
            // We thus nvestigate whether we should copy them to Left.
            // Note that, for practical reasons, we have already copied these values to Left's
            // table, and we now decide whether we should reject (i.e., delete) them or not. 
            //

            Right.Index = 0;
            List<int> BlockOfCopiedResetIndices = new List<int>();
            double r = Constants.rogueReset;
            int b = 0;

            for (Left.Index = 0; Left.Index < Left.Table.Rows.Count; Left.Index++)
            {
                bool isCopy = isRowFromRight[Left.Index];
                double x = Convert.ToDouble(Left.Table.Rows[Left.Index]["Measurement"], CultureInfo.InvariantCulture);
                b = BlockOfCopiedResetIndices.Count;

                if ((!isCopy) && (x != r) && (b == 0))
                {
                    // x is not a copied reset value -- thus nothing to add to the block, and 
                    // the block is empty -- thus nothing to process.
                    continue;
                }
                else if ((!isCopy) && (x == r) && (b == 0))
                {
                    // x is a reset value, but a not a copied reset value -- thus nothing to add to the block, and 
                    // the block is empty -- thus nothing to process.
                    continue;
                }
                else if ((isCopy) && (x != r) && (b == 0))
                {
                    // x is a copied non-reset value -- thus nothing to add to the block, and 
                    // the block is empty -- thus nothing to process.
                    continue;
                }
                else if ((isCopy) && (x == r) && (b == 0))
                {
                    // x is a copied reset value, add it to the block.
                    // This is the first index that is inserted to the block,
                    // because the block is empty
                    BlockOfCopiedResetIndices.Add(Left.Index);
                }
                else if ((isCopy) && (x == r) && (b != 0))
                {
                    // x is a copied reset value, add it to the block.
                    // (which already has some other copied reset inices).
                    BlockOfCopiedResetIndices.Add(Left.Index);
                }
                else if ((isCopy) && (x != r) && (b != 0))
                {
                    // x is a copied non-reset value -- thus nothing to add to the block.
                    // However, since the block is non-empty, it means that
                    // we have reached the end of a block of consecutive copied reset values.
                    // Go process the block.
                    HQDataRedundancyAlgorithms.CopiedResetValuesAlgorithm.Execute(Left, Right, isRowFromRight, BlockOfCopiedResetIndices, RejectRow);
                    BlockOfCopiedResetIndices.Clear();
                }
                else if ((!isCopy) && (x != r) && (b != 0))
                {
                    // x is not a copied reset value -- thus nothing to add to the block.
                    // However, since the block is non-empty, it means that
                    // we have reached the end of a block of consecutive copied reset values.
                    // Go process the block.
                    HQDataRedundancyAlgorithms.CopiedResetValuesAlgorithm.Execute(Left, Right, isRowFromRight, BlockOfCopiedResetIndices, RejectRow);
                    BlockOfCopiedResetIndices.Clear();
                }
                else if ((!isCopy) && (x == r) && (b != 0))
                {
                    // x is a reset value, but not a copied reset value -- thus nothing to add to the block.
                    // However, since the block is non-empty, it means that
                    // we have reached the end of a block of consecutive copied reset values.
                    // Go process the block.
                    HQDataRedundancyAlgorithms.CopiedResetValuesAlgorithm.Execute(Left, Right, isRowFromRight, BlockOfCopiedResetIndices, RejectRow);
                    BlockOfCopiedResetIndices.Clear();
                }
            }

            b = BlockOfCopiedResetIndices.Count;

            if (b != 0)
            {
                HQDataRedundancyAlgorithms.CopiedResetValuesAlgorithm.Execute(Left, Right, isRowFromRight, BlockOfCopiedResetIndices, RejectRow);
            }
        }

        static public void MakeIncreasingCounter(MeasurementLocation Counter, double negative_offset)
        {
            //
            // Counters reset oftenly, i.e., one may find consecutive measurements x_{i} and x_{i+1}
            // for which x_{i} > x_{i+1}. This method reconstructs the table of Counter
            // in order to make x_{i} <= x_{i+1}, for all rows i.
            // Upon input, equality x_{i} = x_{i+1} is a possibility and the output preserves it.
            // (Equality happens if the mechanism is idle or if it malfunctioning, 
            // this issue is not treated here).
            // For "negative_offset" see comments in ConstructNewCounterTable().
            // 

            double counter_measurement = Convert.ToDouble(Counter.Table.Rows[0]["Measurement"], CultureInfo.InvariantCulture);
            counter_measurement += Math.Abs(negative_offset);
            Counter.Table.Rows[0]["Measurement"] = counter_measurement;

            double previous_measurement = counter_measurement;

            double current_measurement;
            double difference;

            for (int i = 1; i < Counter.Table.Rows.Count; i++)
            {
                current_measurement = Convert.ToDouble(Counter.Table.Rows[i]["Measurement"], CultureInfo.InvariantCulture);
                current_measurement += Math.Abs(negative_offset);

                if (current_measurement >= previous_measurement)
                {
                    difference = current_measurement - previous_measurement;
                    Counter.Table.Rows[i]["Measurement"] = counter_measurement + difference;
                    counter_measurement += difference;
                }
                else
                {
                    current_measurement -= Math.Abs(negative_offset);
                    negative_offset = 0;

                    Counter.Table.Rows[i]["Measurement"] = counter_measurement + current_measurement;
                    counter_measurement += current_measurement;
                }
                previous_measurement = current_measurement;
            }
        }

        static public void GetBaseCounterValues(MeasurementLocation Left, MeasurementLocation Right, Dictionary<int, double> BaseRightValues)
        {
            // For counter MLs, simply copying over vrows from Right to Left is not good enough.
            // Since counters reset their operation oftenly, it is very common that a (Left, Right) csv pair
            // has different scales of values. However, the value difference between two consecutive
            // Left lines and the choronoligally corresponding value difference between two Right lines
            // should be the same. Thus, apart form identifying Left's missing lines in Right, we should also
            // perform an arithmetic operation to transform Right value scale into Left value scale.
            // In order to do that, for each block of Right lines that we copy from Right to Left, we seek its
            // corresponding "base" right value, i.e., a Right value that is chronologically as close as possible
            // to the Left line after which the insertions are performed.

            for (int j = 0; j <= Left.Table.Rows.Count; j++)
            {
                if (!Left.Missing.ContainsKey(j)) { continue; }

                if (j > 0)
                {
                    Left.CurrDate = Convert.ToDateTime(Left.Table.Rows[j - 1]["Date"]);

                    // We want to find the Right date that is as close as possible to Left.CurrDate.
                    // Note that Missing[j].Rows[n]["ConjugateIndex"] holds the chronologiacally closest 
                    // Right index to Left index, immediately after Left index, that is outside the sampling rate bounds.
                    // Earlier Right indices might thus exist, and we should look for the earliest one.
                    // We thus have to scan backwards:

                    int n = Left.Missing[j].Rows.Count - 1;
                    Right.Index = Convert.ToInt32(Left.Missing[j].Rows[n]["ConjugateIndex"]);

                    Support.GetClosestRightIndexToAGivenLeftDate_ScanBackwards(Left.CurrDate, Right);

                    double measurement = Convert.ToDouble(Right.Table.Rows[Right.Index]["Measurement"], CultureInfo.InvariantCulture);
                    BaseRightValues.Add(j, measurement);
                }
                else
                {
                    Left.CurrDate = Convert.ToDateTime(Left.Table.Rows[0]["Date"]);

                    // This case is special. We want to insert Right counter data rows that are timewise before the 0th row of Left.
                    // We have to take into account that, once these Right rows have been inserted in Left,
                    // they should have smaller counter values than the input 0th Left row.
                    // Our purpose here is to get the Right measurement whose corresponding index is timewise at least 
                    // at the same level with the Left 0th row.

                    // Missing[0] stores all the Right rows that have to be inserted before the 0th Left row.
                    // The 0th row of Missing[0] is a Right row whose timestamp is immediately before 
                    // the timestamp of the 0th Left row. 
                    // We thus have to scan forwards:

                    Right.Index = Convert.ToInt32(Left.Missing[0].Rows[0]["ConjugateIndex"]);

                    Support.GetClosestRightIndexToAGivenLeftDate_ScanForwards(Left.CurrDate, Right);

                    double measurement = Convert.ToDouble(Right.Table.Rows[Right.Index]["Measurement"], CultureInfo.InvariantCulture);
                    BaseRightValues.Add(0, measurement);
                }
            }
        }

        static public double ConstructNewCounterTable(MeasurementLocation Left, Dictionary<int, double> BaseRightValues, MeasurementLocation NewLeft)
        {
            //
            // Similar to ConstructNewTable(), but for counter-type MeasurementLocation.
            // The addition here is "negative_offset" which deals with case where NewLeft's 
            // first row forms a negative value. This negative value has nothing to do with
            // the ship's mechanism -- counter MLs have always positive values.
            // It deals with a special case, which is relevant only if the "autofill" 
            // operation is taking place, i.e., if the Dictionary Missing is non-empty.
            // Even in this case it may happen only when we want to insert new rows before the
            // 0th row of Left. For example:
            // 
            //               Left    |   Right
            //         -------------------------------
            // time1:  |             |    1010       |
            // time2:  |             |    1020       |
            // time3:  |    10       |    1030       |
            // time4:  |    20       |    1040       |
            //  ...    |    ...      |    ...        |
            //         --------------------------------
            //
            // The example above is a very special case and it tells us two things:
            //  * Left did a reset in the beginning its csv, whereas Right did not.
            //  * Left lost some values after its reset (time1 and time2).
            //
            // Autofill process (InspectMissingValues()) informs us that we should add
            // two rows before time3 in the NewLeft. By transforming from Right's scale
            // into Left's scale of counter values we get:
            // 
            //            NewLeft    |   Right
            //         -------------------------------
            // time1:  |   -10       |    1010       |
            // time2:  |    0        |    1020       |
            // time3:  |    10       |    1030       |
            // time4:  |    20       |    1040       |
            //  ...    |    ...      |    ...        |
            //         --------------------------------
            //
            // i.e., the first entry in NewLeft becomes negative. 
            // We store this negative offset and use it in MakeIncreasingCounter()
            // to turn NewLeft in a Counter with no resets.
            // There, Newleft will transform into
            //
            //            NewLeft    
            //         ---------------
            // time1:  |     0       | 
            // time2:  |    10       |
            // time3:  |    20       |
            // time4:  |    30       |
            //  ...    |    ...      |
            //         ----------------
            //

            double negative_offset = double.MaxValue;

            for (Left.Index = 0; Left.Index <= Left.Table.Rows.Count; Left.Index++)
            {
                DateTime date;
                double measurement;

                if (Left.Missing.ContainsKey(Left.Index))
                {
                    for (int k = Left.Missing[Left.Index].Rows.Count - 1; k >= 0; k--)
                    {
                        date = Convert.ToDateTime(Left.Missing[Left.Index].Rows[k]["Date"]);

                        double rightMeasurement = Convert.ToDouble(Left.Missing[Left.Index].Rows[k]["Measurement"], CultureInfo.InvariantCulture);
                        double baseRightMeasurement = BaseRightValues[Left.Index];
                        double baseLeftMeasurement;

                        if (Left.Index > 0)
                        {
                            baseLeftMeasurement = Convert.ToDouble(Left.Table.Rows[Left.Index - 1]["Measurement"], CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            baseLeftMeasurement = Convert.ToDouble(Left.Table.Rows[0]["Measurement"], CultureInfo.InvariantCulture);
                        }

                        measurement = baseLeftMeasurement + rightMeasurement - baseRightMeasurement;

                        NewLeft.Table.Rows.Add(date, measurement);

                        if ((measurement < 0) && (negative_offset > 0)) { negative_offset = measurement; }
                    }
                }

                if (Left.Index == Left.Table.Rows.Count)
                {
                    negative_offset = (negative_offset < 0) ? negative_offset : 0;
                    return (negative_offset);
                }

                date = Convert.ToDateTime(Left.Table.Rows[Left.Index]["Date"]);
                measurement = Convert.ToDouble(Left.Table.Rows[Left.Index]["Measurement"], CultureInfo.InvariantCulture);

                NewLeft.Table.Rows.Add(date, measurement);
            }

            negative_offset = (negative_offset < 0) ? negative_offset : 0;
            return (negative_offset);
        }

        static public void InspectUnchangedCounterValues(MeasurementLocation Left, MeasurementLocation Right)
        {
            //
            // This is similar to InspectOwnResetValues(), but for counters.
            // Note that we don't distinguish between "Own" and "Copied" in this case
            // (Not entirely sure if we should).
            // In this case, the equivalent of reset values is the case where
            // consecutive measurements x_{i-1} and x_{i} satisfy x_{i-1} = x_{i},
            // which translates to "0" increment in the flow measurement location.
            //
            // The input tables Left and Right should be at least constant in their sequence of values,
            // i.e., they should satisfy x_{i-1} <= x_{i}, for all rows i in Left and
            // y_{j-1} <= y_{j}, for all rows j in Right.
            //
            // We have to inspect:
            // Whenever x_{i-1} = x_{i} in Left, do we also find y_{j} = y_{j'} in Right
            // for sufficiently small timespans |t_{i-1} - t_{j}| and |t_{i) - t_{j'}|, with j'>=j
            // in their corresponding tables?
            // If yes, then all is good. 
            // If not, i.e., if x_{i-1} = x_{i} and y_{j} < y_{j'}
            // then we perform the correction:
            //      x_{i} := x_{i} + y_{j'} - y_{j}
            //
            // The "accumulation" in the code below ensures that a Left value
            // gets updated with all the corrections that occured before it.
            //

            Right.Index = 0;
            Right.CurrValue = Convert.ToDouble(Right.Table.Rows[0]["Measurement"], CultureInfo.InvariantCulture);

            Left.CurrDate = Convert.ToDateTime(Left.Table.Rows[0]["Date"]);
            HQDataRedundancyAlgorithms.UnchangedCounterValuesAlgorithm.GetClosestRightToAGivenLeft(Left, Right);
            double accumulation = 0.0;
            double dummy;

            for (Left.Index = 1; Left.Index < Left.Table.Rows.Count; Left.Index++)
            {
                Left.PrevValue = Convert.ToDouble(Left.Table.Rows[Left.Index - 1]["Measurement"], CultureInfo.InvariantCulture);

                dummy = Convert.ToDouble(Left.Table.Rows[Left.Index]["Measurement"], CultureInfo.InvariantCulture);
                dummy += accumulation;
                Left.Table.Rows[Left.Index]["Measurement"] = dummy;
                Left.CurrValue = dummy;

                if (Left.CurrValue > Left.PrevValue) { continue; }

                Left.PrevDate = Convert.ToDateTime(Left.Table.Rows[Left.Index - 1]["Date"]);
                Left.CurrDate = Convert.ToDateTime(Left.Table.Rows[Left.Index]["Date"]);

                HQDataRedundancyAlgorithms.UnchangedCounterValuesAlgorithm.GetClosestRightToAGivenLeft(Left, Right);

                if (!HQDataRedundancyAlgorithms.UnchangedCounterValuesAlgorithm.ProceedToReplacement(Left, Right)) { continue; }

                HQDataRedundancyAlgorithms.UnchangedCounterValuesAlgorithm.DecideUnchangedValueReplacement(Left, Right, ref accumulation);
            }
        }

    }


    class Program
    {
        public static void Main()
        {
            string[] testDirectories = Directory.GetDirectories("C:\\Users\\spyros.martzoukos\\Desktop\\HQdataRedundancyTEST\\HQDR_V2\\testHQ\\datasets\\Racine\\");

            for (int experiment = 0; experiment < testDirectories.Length; experiment++)
            {
                // Mycode: string DbConnString = "User Id=userfoo;Password=prisma;Data Source=//localhost:1521/xe";
                // Mycode: string DbConnString = "User Id=laros_ermaf;Password=laros;Data Source=//83.212.93.252:1521/LAROS";

                Conjugacy main = new Conjugacy();
                Conjugacy red = new Conjugacy();

                main.Lexicon.Add("0013A200416B41D9_12", new List<string> { "0013A200416B41DD_10", "60000", "1" });
                red.Lexicon.Add("0013A200416B41DD_10", new List<string> { "0013A200416B41D9_12", "60000", "1" });

                main.Lexicon.Add("0013A200416B41D9_13", new List<string> { "0013A200416B41DD_11", "60000", "0" });
                red.Lexicon.Add("0013A200416B41DD_11", new List<string> { "0013A200416B41D9_13", "60000", "0" });

                // Mycode: DataAccess.ReadLexiconsInMemory(DbConnString, main.Lexicon, red.Lexicon);
                // *** ReadLexiconsInMemory(main.Lexicon, red.Lexicon);

                if (Conjugacy.ActiveCSV.Count > 0)
                {
                    Conjugacy.ActiveCSV.Clear();
                }

                string testdir = testDirectories[experiment];

                HQDataRedundancyBackbone.SetExperimentDirectory(testdir);
                HQDataRedundancyBackbone.IdentifyActiveCSVs(Conjugacy.ActiveCSV, main, red);
                HQDataRedundancyBackbone.Execute(Conjugacy.ActiveCSV, main, red);
            }
            Console.WriteLine("END");
            Console.ReadLine();
        }
    }
}
